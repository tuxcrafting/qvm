{
open Parser
}

let ident = ['A'-'Z' 'a'-'z' '_']['A'-'Z' 'a'-'z' '0'-'9' '_']*

rule token = parse
| '\n'+ { EOL }
| [' ' '\t'] { token lexbuf }
| '#' [^ '\n']+ { token lexbuf }
| ident as s { IDENT s }
| (ident as s) ':' { LABEL s }
| '"' ([^ '"']+ as s) '"' { STRING s }
| ('0' 'x' ['0'-'9' 'A'-'F' 'a'-'f']+) as i { INT (Int64.of_string i) }
| ('0' 'b' ['0' '1']+) as i { INT (Int64.of_string i) }
| ['0'-'9']+ as i { INT (Int64.of_string i) }
| '\'' (_ as c) '\'' { INT (Int64.of_int (int_of_char c)) }
| '.' { PC }
| ',' { COMMA }
| '=' { EQUAL }
| ':' { COLON }
| '+' { PLUS }
| '-' { MINUS }
| '*' { TIMES }
| '(' { LPAR }
| ')' { RPAR }
| eof { EOF }
