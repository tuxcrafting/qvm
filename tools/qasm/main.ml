open Ast

let rec gen_loop lexbuf bytes labels fix pc =
  let ast = Parser.stmt Lexer.token lexbuf in
  let b, p = Codegen.gen_code ast bytes labels fix pc in
  if ast != EOF then
    gen_loop lexbuf b labels fix p
  else
    bytes

let () =
  let bytes = Bytes.empty in
  let labels = Hashtbl.create 16 in
  let fix = ref [] in
  let pc = 0 in
  let bytes = gen_loop (Lexing.from_channel stdin) bytes labels fix pc in
  Codegen.fixup bytes labels !fix;
  print_string (Bytes.to_string bytes);
  flush stdout
