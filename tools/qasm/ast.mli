open Opcodes

type ast = Int of int64
         | Ident of string
         | PC
         | Plus of ast * ast
         | Minus of ast * ast
         | Times of ast * ast
         | Label of string * ast
         | Register of int
         | Expr of ast
         | Instr of opcode * ast list
         | EOF
         | None
