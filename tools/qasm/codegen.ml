open Ast
open Opcodes

exception Unknown_instruction of string
exception Label_not_found of string
exception Too_much_immediates

let rec eval_expr x labels fix pc pos =
  match x with
  | Int i -> i
  | Ident s ->
     (try
        Hashtbl.find labels s
      with Not_found ->
        (if pos = -1 then
           raise (Label_not_found s)
         else
           fix := (!fix @ [(pos, s)]);
         Int64.zero))
  | PC -> Int64.of_int pc
  | Plus (x, y) -> Int64.add (eval_expr x labels fix pc pos) (eval_expr y labels fix pc pos)
  | Minus (x, y) -> Int64.sub (eval_expr x labels fix pc pos) (eval_expr y labels fix pc pos)
  | Times (x, y) -> Int64.mul (eval_expr x labels fix pc pos) (eval_expr y labels fix pc pos)
  | _ -> Int64.zero

let mask8 = Int64.of_int 255
let int64_8 x = char_of_int (Int64.to_int (Int64.logand x mask8))

let as_bytes n big =
  let b = Bytes.create (if big then 8 else 4) in
  Bytes.set b 0 (int64_8 n);
  Bytes.set b 1 (int64_8 (Int64.shift_right_logical n 8));
  Bytes.set b 2 (int64_8 (Int64.shift_right_logical n 16));
  Bytes.set b 3 (int64_8 (Int64.shift_right_logical n 24));
  if big then (
    Bytes.set b 4 (int64_8 (Int64.shift_right_logical n 32));
    Bytes.set b 5 (int64_8 (Int64.shift_right_logical n 40));
    Bytes.set b 6 (int64_8 (Int64.shift_right_logical n 48));
    Bytes.set b 7 (int64_8 (Int64.shift_right_logical n 56))
  );
  b

let parse_opr x labels fix pc pos big =
  match x with
  | Register r -> (r, Bytes.empty)
  | Expr e -> (255, as_bytes (eval_expr e labels fix pc pos) big)
  | _ -> (0, Bytes.empty)

let parse_opr_opt x labels fix pc pos =
  match x with
  | Some x -> parse_opr x labels fix pc pos false
  | None -> (0, Bytes.empty)

let parse_opr_opt_n x labels fix pc pos =
  match x with
  | Some x ->
    (match x with
     | Expr x -> eval_expr x labels fix pc pos
     | _ -> Int64.zero)
  | None -> Int64.zero

let data_ins bytes a d labels fix pc big =
  let b = Bytes.create ((List.length a) * d) in
  let rec f l i =
    match l with
    | [] -> (Bytes.cat bytes b, pc + Bytes.length b)
    | x :: xs ->
       let resr, resb = parse_opr x labels fix pc (pc + i) big in
       Bytes.blit resb 0 b i d;
       f xs (i + d) in
  f a 0

let gen_code ast bytes labels fix pc =
  match ast with
  | Label (s, x) -> Hashtbl.add labels s (eval_expr x labels fix pc (-1)); (bytes, pc)
  | Instr (o, a) ->
     (match o with
      | Opcode1 x ->
         let rd, rdv = parse_opr_opt (List.nth_opt a 0) labels fix pc (pc + 4) in
         let rs1, rs1v = parse_opr_opt (List.nth_opt a 1) labels fix pc (pc + 4) in
         let rs2, rs2v = parse_opr_opt (List.nth_opt a 2) labels fix pc (pc + 4) in
         let b = Bytes.create 4 in
         Bytes.set b 0 (char_of_int x);
         Bytes.set b 1 (char_of_int rd);
         Bytes.set b 2 (char_of_int rs1);
         Bytes.set b 3 (char_of_int rs2);
         let b = Bytes.concat Bytes.empty [b; rdv; rs1v; rs2v] in
         if (Bytes.length b) > 8 then
           raise Too_much_immediates
         else
           (Bytes.cat bytes b, pc + Bytes.length b)
      | Opcode2 (x, y) ->
         let rd, rdv = parse_opr_opt (List.nth_opt a 0) labels fix pc (pc + 4) in
         let rs1, rs1v = parse_opr_opt (List.nth_opt a 1) labels fix pc (pc + 4) in
         let b = Bytes.create 4 in
         Bytes.set b 0 (char_of_int x);
         Bytes.set b 1 (char_of_int rd);
         Bytes.set b 2 (char_of_int rs1);
         Bytes.set b 3 (char_of_int y);
         let b = Bytes.concat Bytes.empty [b; rdv; rs1v] in
         if (Bytes.length b) > 8 then
           raise Too_much_immediates
         else
           (Bytes.cat bytes b, pc + Bytes.length b)
      | Unknown s ->
         match s with
         | "db" -> data_ins bytes a 1 labels fix pc false
         | "dw" -> data_ins bytes a 2 labels fix pc false
         | "dd" -> data_ins bytes a 4 labels fix pc false
         | "dq" -> data_ins bytes a 8 labels fix pc true
         | "align" ->
            let n = Int64.to_int (parse_opr_opt_n (List.nth_opt a 0) labels fix pc pc) in
            let a = (Bytes.length bytes) mod n in
            let b = if a != 0 then Bytes.create (n - a)
                    else Bytes.empty in
            (Bytes.cat bytes b, pc + Bytes.length b)
         | "resb" ->
            let n = Int64.to_int (parse_opr_opt_n (List.nth_opt a 0) labels fix pc pc) in
            (Bytes.cat bytes (Bytes.create n), pc + n)
         | _ -> raise (Unknown_instruction s))
  | _ -> (bytes, pc)

let fixup bytes labels fix =
  List.iter (fun (pos, label) ->
      let n0 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 0))) in
      let n1 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 1))) in
      let n2 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 2))) in
      let n3 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 3))) in
      let n = Int64.logor
                (Int64.logor
                   (Int64.logor n0 (Int64.shift_left n1 8))
                   (Int64.shift_left n2 16))
                (Int64.shift_left n3 24) in
      let n = Int64.add n (Hashtbl.find labels label) in
      Bytes.set bytes (pos + 0) (int64_8 n);
      Bytes.set bytes (pos + 1) (int64_8 (Int64.shift_right_logical n 8));
      Bytes.set bytes (pos + 2) (int64_8 (Int64.shift_right_logical n 16));
      Bytes.set bytes (pos + 3) (int64_8 (Int64.shift_right_logical n 24))) fix
