%{
open Ast
open Opcodes
open Registers

let explode s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) []

let str_to_opr s =
  List.map (fun x -> Expr (Int (Int64.of_int (int_of_char x)))) (explode s)
%}

%token EOF EOL
%token <string> IDENT LABEL
%token <string> STRING
%token <int64> INT
%token PC
%token COMMA COLON EQUAL
%token PLUS MINUS TIMES
%token LPAR RPAR

%left PLUS MINUS
%left TIMES
%right UMINUS

%start stmt
%type <Ast.ast> stmt
%type <Ast.ast list> operand_list
%type <Ast.ast> operand
%type <Ast.ast> expr
%%
stmt :
  IDENT operand_list EOL { Instr ((get_opcode $1), $2) }
| IDENT EQUAL expr EOL { Label ($1, $3) }
| LABEL { Label ($1, PC) }
| EOF { EOF }
| EOL { None }

operand_list :
  { [] }
| operand { [$1] }
| operand COMMA operand_list { $1 :: $3 }
| STRING { str_to_opr $1 }
| STRING COMMA operand_list { (str_to_opr $1) @ $3 }

operand :
  COLON expr { Expr $2 }
| IDENT { Register (get_register $1) }

expr :
  INT { Int $1 }
| IDENT { Ident $1 }
| PC { PC }
| expr PLUS expr { Plus ($1, $3) }
| expr MINUS expr { Minus ($1, $3) }
| expr TIMES expr { Times ($1, $3) }
| MINUS expr %prec UMINUS { Minus ((Int Int64.zero), $2) }
| LPAR expr RPAR { $2 }
%%
