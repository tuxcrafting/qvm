#!/bin/sh

DIR="$(cd "$(dirname "$0")" && pwd)"
echo "Generating $DIR/qasm/opcodes.ml"
"$DIR/qasm/genopcodes.pl" < "$DIR/../src/opcodes.h" > "$DIR/qasm/opcodes.ml"
echo "Generating $DIR/qasm/registers.ml"
"$DIR/qasm/genregisters.pl" > "$DIR/qasm/registers.ml"
echo "Generating $DIR/qdis/opcodes.ml"
"$DIR/qdis/genopcodes.pl" < "$DIR/../src/opcodes.h" > "$DIR/qdis/opcodes.ml"
echo "Generating $DIR/qalw/keywords.ml"
"$DIR/qalw/genkeywords.pl" < "$DIR/qalw/parser.mly" > "$DIR/qalw/keywords.ml"
