open Ast
open Asmgen
open Printf

let procedures = ref StringMap.empty
let records = ref StringMap.empty
let globals = ref StringMap.empty

let rec parse_all lexbuf all =
  let res = Parser.program Lexer.token lexbuf in
  match res with
  | EOF -> all
  | _ -> parse_all lexbuf (all @ [res])

let rec charlist_of_string x =
  String.iter (fun c -> printf ":%d," (int_of_char c)) x;
  printf ":0\n"

let () =
  let all = parse_all (Lexing.from_channel stdin) [] in
  List.iter (fun x -> scan_definitions x procedures records globals) all;
  let all = List.map (fun x -> annotate_types_all x procedures !records !globals [] false) all in
  file_start ();
  prolog "_alw_main";
  List.iter (fun x -> genasm_all x TypeAny !globals []) all;
  epilog "_alw_main";
  prolog "_alw_init_strings";
  List.iteri (fun i x ->
      printf "# string_init %d\n" i;
      printf "\tadd rT0, rPC, :string%d-.+8\n" i;
      printf "\tadd rT1, rT0, rI8\n";
      printf "\tst64 rT0, rT1\n") !strings;
  epilog "_alw_init_strings";
  List.iteri (fun i x ->
      printf "string%d: dq :%d, :0\n\tdb " i (String.length x);
      charlist_of_string x) !strings;
  StringMap.iter (fun k (r, a, b) ->
      match b with
      | Extern s -> printf "proc_%s = %s\n" k s
      | _ ->
         prolog (sprintf "proc_%s" k);
         genasm_all b TypeAny !globals [];
         (match r with
          | Some _ -> pop_reg "rA0"
          | None -> ());
         epilog (sprintf "proc_%s" k)) !procedures;
  StringMap.iter (fun k v -> printf "global_%s: resb :%d\n" k (sizeof v)) !globals
