open Ast
open Printf

let bool_to_int64 b = if b then Int64.minus_one else Int64.zero

let rec constexpr = function
  | Int n -> n
  | Or (x, y) -> Int64.logor (constexpr x) (constexpr y)
  | And (x, y) -> Int64.logand (constexpr x) (constexpr y)
  | CmpLt (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) < 0)
  | CmpGt (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) > 0)
  | CmpLe (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) <= 0)
  | CmpGe (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) >= 0)
  | CmpEq (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) = 0)
  | CmpNe (x, y) -> bool_to_int64 (Int64.compare (constexpr x) (constexpr y) != 0)
  | Shl (x, y) -> Int64.shift_left (constexpr x) (Int64.to_int (constexpr y))
  | Shr (x, y) -> Int64.shift_right (constexpr x) (Int64.to_int (constexpr y))
  | Add (x, y) -> Int64.add (constexpr x) (constexpr y)
  | Sub (x, y) -> Int64.sub (constexpr x) (constexpr y)
  | Mult (x, y) -> Int64.mul (constexpr x) (constexpr y)
  | Div (x, y) -> Int64.div (constexpr x) (constexpr y)
  | Rem (x, y) -> Int64.rem (constexpr x) (constexpr y)
  | Not x -> Int64.lognot (constexpr x)
  | UPlus x -> constexpr x
  | UMinus x -> Int64.neg (constexpr x)
  | _ -> raise (Failure "not a constant expression")

let rec sizeof = function
  | TypeChar -> 1
  | TypeArray _ -> 16
  | TypeStaticArray (l, t) -> Int64.to_int (constexpr l) * sizeof t
  | _ -> 8

let file_start () =
  printf "\talloc rSP, :8192\n";
  printf "\tadd rSP, rSP, :8192\n";
  printf "\tjrl rRA, rPC, :_alw_init_strings-.\n";
  printf "\tjrl rRA, rPC, :_alw_main-.\n";
  printf "\tunimp0\n"

let push_reg r =
  printf "\tsub rSP, rSP, rI8\n";
  printf "\tst64 rSP, %s\n" r
let pop_reg r =
  printf "\tld64 %s, rSP\n" r;
  printf "\tadd rSP, rSP, rI8\n"

let prolog s =
  printf "# prolog %s\n" s;
  printf "%s:\n" s;
  printf "\tfunction rZ, :%s_end-.-8\n" s;
  printf "\tadd\n";
  push_reg "rFP";
  push_reg "rRA";
  printf "\tadd rFP, rSP\n"
let epilog s =
  printf "# epilog %s\n" s;
  pop_reg "rRA";
  pop_reg "rFP";
  printf "\tjrl rZ, rRA\n";
  printf "\tadd\n";
  printf "%s_end:\n" s
let call s n =
  printf "# call %s %d\n" s n;
  let rec pusharg i =
    pop_reg (sprintf "rA%d" i);
    if i != 0 then pusharg (i - 1) in
  if n > 0 then pusharg (n - 1);
  printf "\tjrl rRA, rPC, :proc_%s-.\n" s

let get_global s =
  printf "# get_global %s\n" s;
  printf "\tld64 rT0, rPC, :global_%s-.\n" s;
  push_reg "rT0"
let set_global s =
  printf "# set_global %s\n" s;
  pop_reg "rT0";
  printf "\tst64 rPC, rT0, :global_%s-.\n" s

let goto s =
  printf "# goto %s\n" s;
  printf "\tjrl rZ, rPC, :%s-.\n" s
let goto_false s =
  printf "# goto_false %s\n" s;
  pop_reg "rT0";
  printf "\tbrf :%s-., rT0\n" s

let push_string n =
  printf "# push_string %d\n" n;
  printf "\tadd rT0, rPC, :string%d-.\n" n;
  push_reg "rT0"
let mask32 = Int64.sub (Int64.shift_left Int64.one 32) Int64.one
let maskN31 = Int64.lognot (Int64.sub (Int64.shift_left Int64.one 31) Int64.one)
let push_int n =
  printf "# push_int %s\n" (Int64.to_string n);
  printf "\tadd rT0, :%s\n" (Int64.to_string (Int64.logand n mask32));
  (if not (Int64.equal (Int64.logand n maskN31) Int64.zero) then
     let n = Int64.shift_right_logical n 32 in
     printf "\tsetu rT0, rT0, :%s\n" (Int64.to_string n));
  push_reg "rT0"
let drop () =
  printf "# drop\n";
  printf "\tadd rSP, rSP, rI8\n"
let dup () =
  printf "# dup\n";
  pop_reg "rT0";
  push_reg "rT0";
  push_reg "rT0"

let deref () =
  printf "# deref\n";
  pop_reg "rT0";
  printf "\tld64 rT0, rT0\n";
  push_reg "rT0"

let bin_op s =
  printf "# bin_op %s\n" s;
  pop_reg "rT1";
  pop_reg "rT0";
  printf "\t%s\n" s;
  push_reg "rT0"

let un_op s =
  printf "# un_op %s\n" s;
  pop_reg "rT0";
  printf "\t%s\n" s;
  push_reg "rT0"

let op_f s t =
  if type_eq t TypeReal then sprintf "f%s" s
  else s

let bin_op_f s t =
  bin_op (op_f s t)
let un_op_f s t =
  un_op (op_f s t)

let strings = ref []
let loopbase = Random.self_init (); Random.int ((1 lsl 30) - 1)
let loopid = ref 0

let rec genasm_all x t globals scopes =
  let genasm_t x t =
    genasm_all x t globals scopes in
  let genasm x =
    genasm_t x t in
  match x with
  | Ident s -> if StringMap.mem s globals then get_global s
  | String s -> push_string (List.length !strings); strings := !strings @ [s]
  | Int n -> push_int n
  | Real n -> push_int (Int64.bits_of_float n)
  | Boolean b -> push_int (bool_to_int64 b)
  | Char c -> push_int (Int64.of_int (int_of_char c))
  | RefNull -> push_int Int64.zero
  | Procedure (s, l) ->
     List.iter genasm l;
     call s (List.length l);
     if t != TypeAny then
       push_reg "rA0"
  | Assign (x, y) ->
     genasm y;
     for i = 2 to List.length x do
       dup ()
     done;
     let rec assign = function
       | Ident s -> if StringMap.mem s globals then set_global s
       | Typed (_, x) -> assign x
       | _ -> () in
     List.iter assign x
  | Or (x, y) -> genasm x; genasm y; bin_op "or rT0, rT0, rT1"
  | And (x, y) -> genasm x; genasm y; bin_op "and rT0, rT0, rT1"
  | CmpLt (x, y) -> genasm x; genasm y; bin_op_f "lt rT0, rT0, rT1" t
  | CmpGt (x, y) -> genasm x; genasm y; bin_op_f "lt rT0, rT1, rT0" t
  | CmpLe (x, y) -> genasm x; genasm y; bin_op_f "ge rT0, rT1, rT0" t
  | CmpGe (x, y) -> genasm x; genasm y; bin_op_f "ge rT0, rT0, rT1" t
  | CmpEq (x, y) -> genasm x; genasm y; bin_op "eq rT0, rT0, rT1"
  | CmpNe (x, y) -> genasm x; genasm y; bin_op "neq rT0, rT0, rT1"
  | Shl (x, y) -> genasm x; genasm y; bin_op "lshift rT0, rT0, rT1"
  | Shr (x, y) -> genasm x; genasm y; bin_op "rashift rT0, rT0, rT1"
  | Add (x, y) -> genasm x; genasm y; bin_op_f "add rT0, rT0, rT1" t
  | Sub (x, y) -> genasm x; genasm y; bin_op_f "sub rT0, rT0, rT1" t
  | Mult (x, y) -> genasm x; genasm y; bin_op_f "mul rT0, rT0, rT1" t
  | Div (x, y) -> genasm x; genasm y; bin_op_f "div rT0, rT0, rT1" t
  | Rem (x, y) -> genasm x; genasm y; bin_op_f "rem rT0, rT0, rT1" t
  | Not x -> genasm x; un_op "xor rT0, rT0, rN1"
  | UPlus x -> genasm x
  | UMinus x -> genasm x; un_op_f "sub rT0, rZ, rT0" t
  | Deref x -> genasm x; deref ()
  | Sizeof t -> push_int (Int64.of_int (sizeof t))
  | If (c, a, None) ->
     genasm c;
     goto_false (sprintf "end_%d_%d" loopbase !loopid);
     genasm a;
     printf "end_%d_%d:\n" loopbase !loopid;
     loopid := !loopid + 1
  | If (c, a, Some b) ->
     genasm c;
     goto_false (sprintf "else_%d_%d" loopbase !loopid);
     genasm a;
     goto (sprintf "end_%d_%d" loopbase !loopid);
     printf "else_%d_%d:\n" loopbase !loopid;
     genasm b;
     printf "end_%d_%d:\n" loopbase !loopid;
     loopid := !loopid + 1
  | IfExpr (a, b, c) -> genasm_t (If (a, b, c)) t
  | While (c, b) ->
     printf "while_%d_%d:\n" loopbase !loopid;
     genasm c;
     goto_false (sprintf "end_%d_%d" loopbase !loopid);
     genasm b;
     goto (sprintf "while_%d_%d" loopbase !loopid);
     printf "end_%d_%d:\n" loopbase !loopid;
     loopid := !loopid + 1
  | Block (_, b, _) -> List.iter genasm b
  | SelectExpr (x, y) -> genasm x; genasm y
  | Expr x -> genasm x; drop ()
  | Typed (t, x) -> genasm_t x t
  | Asm s -> printf "# asm\n%s\n# end_asm\n" s
  | _ -> ()
