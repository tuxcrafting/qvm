%{
open Ast

exception Expected_lvalue

let check_lvalue x =
  if not (is_lvalue x) then raise Expected_lvalue
%}

%token EOF
%token <string> IDENT STRING
%token <int64> INT
%token <float> REAL
%token <char> CHAR

%token KW_STRING KW_INTEGER KW_REAL KW_BOOLEAN KW_CHAR
%token KW_REFERENCE KW_ARRAY KW_STATIC KW_RECORD
%token KW_TYPE KW_PROCEDURE KW_BEGIN KW_END
%token KW_IF KW_IFEXPR KW_THEN KW_ELSE
%token KW_WHILE KW_DO KW_FOR KW_TO KW_BY
%token KW_TRUE KW_FALSE KW_NULL
%token KW_AND KW_OR KW_SHL KW_SHR KW_REM
%token KW_REF KW_NEW KW_DELETE
%token KW_SIZEOF KW_CAST KW_LENGTH
%token KW_EXTERN KW_ASM

%token LPAR RPAR LSQR RSQR
%token COMMA SEMICOLON COLON ARROW
%token ASSIGN

%token PLUS MINUS TIMES DIV EXP
%token NOT LT GT LE GE EQ NE
%token DEREF

%right ASSIGN
%right KW_IF
%left KW_OR
%left KW_AND
%nonassoc LT, LE, EQ, NE, GE, GT, KW_IS
%left KW_SHL KW_SHR
%left PLUS MINUS
%left TIMES DIV KW_REM
%right EXP
%right UMINUS UPLUS NOT KW_REF DEREF KW_NEW KW_SIZEOF KW_LENGTH KW_POINTER KW_CAST
%left LSQR
%left ARROW

%start program
%type <Ast.ast> program
%%
program :
  stmt SEMICOLON { $1 }
| var_decl SEMICOLON { $1 }
| proc_proper_decl SEMICOLON { $1 }
| proc_func_decl SEMICOLON { $1 }
| record_decl SEMICOLON { $1 }
| EOF { EOF }

stmt :
  { Empty }
| assign { $1 }
| proc { $1 }
| block_body KW_END { Block (ref StringMap.empty, $1, true) }
| KW_DELETE expr { Delete $2 }
| KW_NULL expr { Expr $2 }
| KW_IF expr KW_THEN stmt %prec KW_IF { If ($2, $4, None) }
| KW_IF expr KW_THEN stmt KW_ELSE stmt %prec KW_IF { If ($2, $4, Some $6) }
| KW_WHILE expr KW_DO stmt { While ($2, $4) }
| KW_FOR expr ASSIGN expr KW_TO expr for_by KW_DO stmt
  { check_lvalue $2; expand_for $2 $4 $6 $7 $9 }
| KW_ASM STRING { Asm $2 }
| IDENT { Ident $1 }

for_by :
  { (Int Int64.one) }
| KW_BY expr { $2 }

var_decl :
  etype id_list { VarDecl ($1, $2) }

proc_proper_decl :
  KW_PROCEDURE IDENT field_list_opt SEMICOLON proc_proper_body
  { ProcedureDecl (ref StringMap.empty, None, $2, $3, $5) }

proc_func_decl :
  etype KW_PROCEDURE IDENT field_list_opt SEMICOLON proc_func_body
  { ProcedureDecl (ref StringMap.empty, Some $1, $3, $4, $6) }

proc_proper_body :
  extern { $1 }
| stmt { $1 }

proc_func_body :
  extern { $1 }
| expr { $1 }
| block_body expr KW_END { SelectExpr ((Block (ref StringMap.empty, $1, false)), $2) }

proc :
  IDENT LPAR expr_list RPAR { Procedure ($1, $3) }

block_head :
  KW_BEGIN { [] }
| block_head var_decl SEMICOLON { $1 @ [$2] }

block_body :
  block_head { $1 }
| block_body stmt SEMICOLON { $1 @ [$2] }

extern :
  KW_EXTERN STRING { Extern $2 }

assign :
  expr_list ASSIGN expr { List.iter check_lvalue $1; Assign ($1, $3) }

record_decl :
  KW_RECORD IDENT field_list_opt { RecordDecl ($2, $3) }

field_list_opt :
  { [] }
| LPAR field_list RPAR { $2 }

field_list :
  field { $1 }
| field SEMICOLON field_list { $1 @ $3 }

field :
  etype id_list { List.map (fun x -> ($1, x)) $2 }

expr_list :
  expr { [$1] }
| expr COMMA expr_list { $1 :: $3 }

named_expr_list_opt :
  { [] }
| LPAR named_expr_list RPAR { $2 }

named_expr_list :
  named_expr { [$1] }
| named_expr COMMA named_expr_list { $1 :: $3 }

named_expr :
  IDENT COLON expr { ($1, $3) }

id_list :
  IDENT { [$1] }
| IDENT COMMA id_list { $1 :: $3 }

expr :
  IDENT { Ident $1 }
| STRING { String $1 }
| INT { Int $1 }
| REAL { Real $1 }
| KW_TRUE { Boolean true }
| KW_FALSE { Boolean false }
| KW_NULL { RefNull }
| CHAR { Char $1 }
| KW_RECORD IDENT named_expr_list_opt { Record ($2, $3) }
| expr LSQR expr RSQR %prec LSQR { Subscript ($1, $3) }
| expr KW_OR expr { Or ($1, $3) }
| expr KW_AND expr { And ($1, $3) }
| expr LT expr { CmpLt ($1, $3) }
| expr GT expr { CmpGt ($1, $3) }
| expr LE expr { CmpLe ($1, $3) }
| expr GE expr { CmpGe ($1, $3) }
| expr EQ expr { CmpEq ($1, $3) }
| expr NE expr { CmpNe ($1, $3) }
| expr KW_SHL expr { Shl ($1, $3) }
| expr KW_SHR expr { Shr ($1, $3) }
| expr PLUS expr { Add ($1, $3) }
| expr MINUS expr { Sub ($1, $3) }
| expr TIMES expr { Mult ($1, $3) }
| expr DIV expr { Div ($1, $3) }
| expr KW_REM expr { Rem ($1, $3) }
| expr EXP expr { Exp ($1, $3) }
| NOT expr { Not $2 }
| PLUS expr %prec UPLUS { UPlus $2 }
| MINUS expr %prec UMINUS { UMinus $2 }
| LPAR expr RPAR { $2 }
| KW_REF expr { Ref $2 }
| DEREF expr { Deref $2 }
| KW_LENGTH expr { Length $2 }
| proc { $1 }
| KW_NEW LPAR etype RPAR %prec KW_NEW { New $3 }
| KW_SIZEOF LPAR etype RPAR %prec KW_SIZEOF { Sizeof $3 }
| KW_CAST LPAR etype RPAR expr %prec KW_CAST { Cast ($5, $3) }
| expr ARROW IDENT %prec ARROW { Member ($1, $3) }
| KW_IFEXPR expr KW_THEN expr %prec KW_IF { IfExpr ($2, $4, None) }
| KW_IFEXPR expr KW_THEN expr KW_ELSE expr %prec KW_IF { IfExpr ($2, $4, (Some $6)) }

etype :
  KW_STRING { ast_TypeString }
| KW_STRING LPAR expr RPAR { ast_TypeStaticString $3 }
| KW_INTEGER { TypeInteger }
| KW_REAL { TypeReal }
| KW_BOOLEAN { TypeBoolean }
| KW_CHAR { TypeChar }
| KW_REFERENCE LPAR etype RPAR { TypeReference $3 }
| KW_REFERENCE LPAR TIMES RPAR { TypeReference TypeAny }
| KW_ARRAY LPAR etype RPAR { TypeArray $3 }
| KW_STATIC KW_ARRAY LPAR expr COMMA etype RPAR { TypeStaticArray ($4, $6) }
| KW_RECORD IDENT { TypeRecord $2 }
%%
