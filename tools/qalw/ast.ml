module StringMap = Map.Make(String)

type ast = TypeAny
         | TypeInteger
         | TypeReal
         | TypeBoolean
         | TypeChar
         | TypeReference of ast
         | TypeArray of ast
         | TypeStaticArray of ast * ast
         | TypeRecord of string
         | Typed of ast * ast
         | Ident of string
         | String of string
         | Int of int64
         | Real of float
         | Boolean of bool
         | Char of char
         | RefNull
         | Record of string * ((string * ast) list)
         | Procedure of string * (ast list)
         | Subscript of ast * ast
         | Assign of (ast list) * ast
         | Or of ast * ast
         | And of ast * ast
         | CmpLt of ast * ast
         | CmpGt of ast * ast
         | CmpLe of ast * ast
         | CmpGe of ast * ast
         | CmpEq of ast * ast
         | CmpNe of ast * ast
         | Shl of ast * ast
         | Shr of ast * ast
         | Add of ast * ast
         | Sub of ast * ast
         | Mult of ast * ast
         | Div of ast * ast
         | Rem of ast * ast
         | Exp of ast * ast
         | Not of ast
         | UPlus of ast
         | UMinus of ast
         | Ref of ast
         | Deref of ast
         | Length of ast
         | New of ast
         | Delete of ast
         | Sizeof of ast
         | Cast of ast * ast
         | Member of ast * string
         | VarDecl of ast * (string list)
         | ProcedureDecl of ast StringMap.t ref
                            * (ast option)
                            * string
                            * ((ast * string) list)
                            * ast
         | RecordDecl of string * ((ast * string) list)
         | If of ast * ast * (ast option)
         | IfExpr of ast * ast * (ast option)
         | While of ast * ast
         | Block of ast StringMap.t ref * (ast list) * bool
         | Expr of ast
         | SelectExpr of ast * ast
         | Extern of string
         | Asm of string
         | Empty
         | EOF

let ast_TypeString = TypeArray TypeChar
let ast_TypeStaticString n = TypeStaticArray (n, TypeChar)

let ast_Or x y = Or (x, y)
let ast_And x y = And (x, y)
let ast_CmpLt x y = CmpLt (x, y)
let ast_CmpGt x y = CmpGt (x, y)
let ast_CmpLe x y = CmpLe (x, y)
let ast_CmpGe x y = CmpGe (x, y)
let ast_CmpEq x y = CmpEq (x, y)
let ast_CmpNe x y = CmpNe (x, y)
let ast_Shl x y = Shl (x, y)
let ast_Shr x y = Shr (x, y)
let ast_Add x y = Add (x, y)
let ast_Sub x y = Sub (x, y)
let ast_Mult x y = Mult (x, y)
let ast_Div x y = Div (x, y)
let ast_Rem x y = Rem (x, y)
let ast_Exp x y = Exp (x, y)
let ast_Not x = Not x
let ast_UPlus x = UPlus x
let ast_UMinus x = UMinus x

let expand_for v f t s b =
  Block (ref StringMap.empty,
         [Assign ([v], f);
          While (CmpLt (v, t),
                 Block (ref StringMap.empty, [b; (Assign ([v], Add (v, s)))], false))], false)

let is_lvalue = function
  | Ident _ -> true
  | Member (_, _) -> true
  | Deref _ -> true
  | Subscript (_, _) -> true
  | _ -> false

let scan_definitions x procedures records globals =
  match x with
  | VarDecl (t, s) -> List.iter (fun x -> globals := StringMap.add x t !globals) s
  | ProcedureDecl (v, t, s, a, b) ->
     List.iter (fun (x, y) -> v := StringMap.add y x !v) a;
     procedures := StringMap.add s (t, List.map (fun (x, y) -> x) a, Empty) !procedures
  | RecordDecl (s, f) ->
     let t = ref StringMap.empty in
     records := StringMap.add s t !records;
     List.iter (fun (x, y) -> t := StringMap.add y x !t) f
  | _ -> ()

exception Type_error of string

let rec type_eq x y =
  match (x, y) with
  | (TypeReference x, TypeReference y) -> type_eq x y
  | (TypeAny, _) -> true
  | (_, TypeAny) -> true
  | (x, y) -> x = y
let rec type_main x y =
  match (x, y) with
  | (TypeReference x, TypeReference y) -> TypeReference (type_main x y)
  | (TypeAny, _) -> y
  | (x, y) -> x
let get_type = function
  | Typed (t, _) -> t
  | _ -> TypeAny

let compatible_cast s d =
  match (s, d) with
  | (TypeReference _, TypeInteger) -> true
  | (TypeInteger, TypeReference _) -> true
  | (TypeReal, TypeInteger) -> true
  | (TypeInteger, TypeReal) -> true
  | (TypeChar, TypeInteger) -> true
  | (TypeInteger, TypeChar) -> true
  | (TypeArray x, TypeReference y) -> type_eq x y
  | (TypeStaticArray (_, x), TypeReference y) -> type_eq x y
  | (TypeReference x, TypeStaticArray (_, y)) -> x != TypeAny && (type_eq x y)
  | (x, y) -> type_eq x y

let option_map f x =
  match x with
  | Some x -> Some (f x)
  | None -> None

let rec annotate_types_all elm procedures records globals scopes in_expr =
  let annotate_types elm =
    annotate_types_all elm procedures records globals scopes in_expr in
  let annotate_types_e elm in_expr =
    annotate_types_all elm procedures records globals scopes in_expr in
  let annotate_1 c x types =
    let x = annotate_types x in
    let t = get_type x in
    if not (List.exists (fun x -> type_eq x t) types) then
      raise (Type_error "invalid type");
    Typed (t, c x) in
  let annotate_2 c x y types =
    let x = annotate_types x in
    let y = annotate_types y in
    let tx = get_type x in
    let ty = get_type y in
    if not (type_eq tx ty) then
      raise (Type_error "types do not match");
    let t = type_main tx ty in
    if not (List.exists (fun x -> type_eq x t) types) then
      raise (Type_error "invalid type");
    Typed (t, c x y) in
  let annotate_2_bool c x y types =
    match annotate_2 c x y types with
    | Typed (t, x) -> Typed (TypeBoolean, x)
    | _ -> raise (Failure "this shouldn't happen") in
  let find_var s =
    if StringMap.mem s globals then
      StringMap.find s globals
    else
      StringMap.find s !(List.find (fun x -> StringMap.mem s !x) scopes) in
  match elm with
  | Ident s ->
     if StringMap.mem s !procedures then
       annotate_types (Procedure (s, []))
     else if not in_expr then
       raise (Type_error "expression cannot be top level")
     else Typed (find_var s, elm)
  | String _ -> Typed (ast_TypeString, elm)
  | Int _ -> Typed (TypeInteger, elm)
  | Real _ -> Typed (TypeReal, elm)
  | Boolean _ -> Typed (TypeBoolean, elm)
  | Char _ -> Typed (TypeChar, elm)
  | RefNull -> Typed (TypeReference TypeAny, elm)
  | Record (s, a) ->
     let r = StringMap.find s records in
     let a = List.map (fun (x, y) ->
         let y = annotate_types y in
         let t = StringMap.find x !r in
         if not (type_eq (get_type y) t) then
           raise (Type_error "types do not match")
         else (x, y)) a in
     Typed (TypeRecord s, Record (s, a))
  | Procedure (s, a) ->
     let d = StringMap.find s !procedures in
     let r, l, _ = d in
     let a = List.map (fun x -> annotate_types_e x true) a in
     if not (List.for_all2 (fun x y -> type_eq x (get_type y)) l a) then
       raise (Type_error "types do not match");
     (match r with
      | Some x ->
         Typed (x, Procedure (s, a))
      | None ->
         if in_expr then
           raise (Type_error "proper function used in expression")
         else Procedure (s, a))
  | Subscript (x, y) ->
     let x = annotate_types x in
     let y = annotate_types y in
     if not (type_eq (get_type y) TypeInteger) then
       raise (Type_error "index must be integer");
     Typed ((match get_type x with
             | TypeReference t ->
                if t = TypeAny then
                  raise (Type_error "cannot subscript a REFERENCE(*)")
                else t
             | TypeArray t -> t
             | TypeStaticArray (_, t) -> t
             | _ -> raise (Type_error "can only subscript array or reference")), Subscript (x, y))
  | Assign (x, y) ->
     let x = List.map (fun x -> annotate_types_e x true) x in
     let y = annotate_types_e y true in
     let t = get_type (List.hd x) in
     if not (List.for_all (fun x -> type_eq (get_type x) t) (List.tl x)) then
       raise (Type_error "types do not match");
     if not (type_eq (get_type y) t) then
       raise (Type_error "types do not match");
     Assign (x, y)
  | Or (x, y) -> annotate_2 ast_Or x y [TypeInteger; TypeBoolean; TypeChar]
  | And (x, y) -> annotate_2 ast_And x y [TypeInteger; TypeBoolean; TypeChar]
  | CmpLt (x, y) -> annotate_2_bool ast_CmpLt x y [TypeInteger; TypeReal]
  | CmpGt (x, y) -> annotate_2_bool ast_CmpGt x y [TypeInteger; TypeReal]
  | CmpLe (x, y) -> annotate_2_bool ast_CmpLe x y [TypeInteger; TypeReal]
  | CmpGe (x, y) -> annotate_2_bool ast_CmpGe x y [TypeInteger; TypeReal]
  | CmpEq (x, y) -> annotate_2_bool ast_CmpEq x y [TypeInteger; TypeReal; TypeBoolean; TypeChar; TypeReference TypeAny]
  | CmpNe (x, y) -> annotate_2_bool ast_CmpNe x y [TypeInteger; TypeReal; TypeBoolean; TypeChar; TypeReference TypeAny]
  | Shl (x, y) -> annotate_2 ast_Shl x y [TypeInteger; TypeChar]
  | Shr (x, y) -> annotate_2 ast_Shr x y [TypeInteger; TypeChar]
  | Add (x, y) -> annotate_2 ast_Add x y [TypeInteger; TypeReal; TypeChar; TypeReference TypeAny]
  | Sub (x, y) -> annotate_2 ast_Sub x y [TypeInteger; TypeReal; TypeChar; TypeReference TypeAny]
  | Mult (x, y) -> annotate_2 ast_Mult x y [TypeInteger; TypeReal; TypeChar]
  | Div (x, y) -> annotate_2 ast_Div x y [TypeInteger; TypeReal; TypeChar]
  | Rem (x, y) -> annotate_2 ast_Rem x y [TypeInteger; TypeReal; TypeChar]
  | Exp (x, y) -> annotate_2 ast_Exp x y [TypeReal]
  | Not x -> annotate_1 ast_Not x [TypeInteger; TypeBoolean; TypeChar]
  | UPlus x -> annotate_1 ast_UPlus x [TypeInteger; TypeReal; TypeChar]
  | UMinus x -> annotate_1 ast_UMinus x [TypeInteger; TypeReal; TypeChar]
  | Ref x ->
     let x = annotate_types x in
     Typed (TypeReference (get_type x), Ref x)
  | Deref x ->
     let x = annotate_types x in
     (match get_type x with
      | TypeReference t ->
         if t = TypeAny then
           raise (Type_error "cannot dereference wildcard reference")
         else Typed (t, Deref x)
      | _ -> raise (Type_error "can only dereference reference"))
  | Length x ->
     let x = annotate_types x in
     (match get_type x with
      | TypeArray t -> Typed (TypeInteger, Length x)
      | TypeStaticArray (l, t) -> annotate_types l
      | _ -> raise (Type_error "can only get length of array"))
  | New t -> Typed (t, elm)
  | Delete x ->
     let x = annotate_types_e x true in
     if not (type_eq (get_type x) (TypeReference TypeAny)) then
       raise (Type_error "can only delete reference")
     else Delete x
  | Sizeof t -> Typed (TypeInteger, elm)
  | Cast (x, t) ->
     let x = annotate_types x in
     if not (compatible_cast (get_type x) t) then
       raise (Type_error "incompatible cast");
     Typed (t, x)
  | Member (x, y) ->
     let x = annotate_types x in
     let rec get_ast x =
       match get_type x with
       | TypeReference r ->
          let x = Typed (r, Deref x) in
          get_ast x
       | TypeRecord s ->
          let r = StringMap.find s records in
          let t = StringMap.find y !r in
          Typed (t, Member (x, y))
       | _ -> raise (Type_error "only records have members") in
     get_ast x
  | VarDecl (t, l) ->
     (if List.length scopes != 0 then
        let scope = List.hd scopes in
        List.iter (fun x -> scope := StringMap.add x t !scope) l);
     VarDecl (t, l)
  | ProcedureDecl (v, r, s, a, b) ->
     let b = annotate_types_all b procedures records globals (v :: scopes) in_expr in
     let dr, da, _ = StringMap.find s !procedures in
     procedures := StringMap.add s (dr, da, b) !procedures;
     ProcedureDecl (v, r, s, a, b)
  | If (c, a, b) ->
     let c = annotate_types_e c true in
     if not (type_eq (get_type c) TypeBoolean) then
       raise (Type_error "condition must be boolean");
     let a = annotate_types a in
     let b = option_map annotate_types b in
     If (c, a, b)
  | IfExpr (c, a, b) ->
     let c = annotate_types c in
     if not (type_eq (get_type c) TypeBoolean) then
       raise (Type_error "condition must be boolean");
     let a = annotate_types a in
     let at = get_type a in
     let b, bt =
       match b with
       | Some b ->
          let b = annotate_types b in
          let bt = get_type b in
          if not (type_eq at bt) then
            raise (Type_error "types do not match")
          else (Some b, bt)
       | None -> (None, at) in
     Typed (type_main at bt, IfExpr (c, a, b))
  | While (c, b) ->
     let c = annotate_types_e c true in
     if not (type_eq (get_type c) TypeBoolean) then
       raise (Type_error "condition must be boolean");
     While (c, annotate_types b)
  | Block (v, x, p) ->
     if p then
       Block (v, List.map (fun x ->
                     annotate_types_all x procedures records globals (v :: scopes) in_expr) x, p)
     else
       Block (v, List.map annotate_types x, p)
  | Expr x -> Expr (annotate_types_e x true)
  | SelectExpr (x, y) ->
     let x = annotate_types x in
     SelectExpr (x, annotate_types_e y true)
  | x -> x
