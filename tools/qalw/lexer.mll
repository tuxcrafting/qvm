{
open Parser
open Keywords
open Str

let dbl_quote_re = regexp "\"\""
let apostrophe_re = regexp "'"
let alwfloat_of_string s =
  float_of_string (replace_first apostrophe_re "e" s)
}

let letter = ['A'-'Z' 'a'-'z']
let digit = ['0'-'9']
let hex_digit = ['0'-'9' 'A'-'F' 'a'-'f']
let integer = digit+
let scale_factor = '\'' ['+' '-']? integer
let unscaled_real = (integer '.' integer) | (integer '.') | ('.' integer)
let real = (unscaled_real scale_factor?) | (integer scale_factor) | scale_factor

rule token = parse
| ['\000'-' '] { token lexbuf }
| '%' [^ '%']* '%' { token lexbuf }
| '@' [^ '\n']* { token lexbuf }
| (letter (letter | digit | '_')*) as s { get_keyword (String.uppercase_ascii s) }
| integer as i { INT (Int64.of_string i) }
| (real as r) { REAL (alwfloat_of_string r) }
| '#' (hex_digit+ as h) { INT (Int64.of_string (Printf.sprintf "0x%s" h)) }
| '"' (([^ '"'] | "\"\"")* as s) '"' { STRING (global_replace dbl_quote_re "\"" s) }
| '?' (_ as c) { CHAR c }
| "?\\" (integer as i) { CHAR (char_of_int (int_of_string i)) }
| '(' { LPAR }
| ')' { RPAR }
| '[' { LSQR }
| ']' { RSQR }
| ',' { COMMA }
| ':' { COLON }
| ';' { SEMICOLON }
| "->" { ARROW }
| ":=" { ASSIGN }
| '+' { PLUS }
| '-' { MINUS }
| '*' { TIMES }
| '/' { DIV }
| "**" { EXP }
| '!' { NOT }
| '<' { LT }
| '>' { GT }
| '=' { EQ }
| "<=" { LE }
| ">=" { GE }
| "!=" { NE }
| '^' { DEREF }
| eof { EOF }
