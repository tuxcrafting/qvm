type operand = Register of int
             | Immediate of int64

let bit31 = Int64.shift_left Int64.one 31
let mask31 = Int64.sub bit31 Int64.one

let decode_int bytes pos =
  let n0 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 0))) in
  let n1 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 1))) in
  let n2 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 2))) in
  let n3 = Int64.of_int (int_of_char (Bytes.get bytes (pos + 3))) in
  let n = Int64.logor
            (Int64.logor
               (Int64.logor n0 (Int64.shift_left n1 8))
               (Int64.shift_left n2 16))
            (Int64.shift_left n3 24) in
  if Int64.equal (Int64.logand n bit31) Int64.zero then
    n
  else
    Int64.pred (Int64.sub Int64.zero (Int64.logand (Int64.lognot n) mask31))

let decode b =
  let opcode = int_of_char (Bytes.get b 0) in
  let rd = int_of_char (Bytes.get b 1) in
  let rs1 = int_of_char (Bytes.get b 2) in
  let rs2 = int_of_char (Bytes.get b 3) in
  let opword = Bytes.sub b 0 4 in
  let immb = Bytes.sub b 4 4 in
  let imm = decode_int immb 0 in
  let immb = if rd = 255 || rs1 = 255 || rs2 = 255 then immb else Bytes.empty in
  let rdp = if rd = 255 then Immediate imm else Register rd in
  let rs1p = if rs1 = 255 then Immediate imm else Register rs1 in
  let rs2p = if rs2 = 255 then Immediate imm else Register rs2 in
  ((Bytes.cat opword immb), opcode, rdp, rs1p, rs2p, rs2)

let fill_buffer ic buf len eof =
  while !len < Bytes.length buf do
    let v = try input_byte ic
            with End_of_file -> Bytes.set eof !len '\xFF'; 0 in
    Bytes.set buf !len (char_of_int v);
    len := !len + 1
  done

let opr_to_str = function
  | Register i -> Printf.sprintf "r%d" i
  | Immediate i -> Printf.sprintf ":%s" (Int64.to_string i)
let opr_to_str_opc opcode r =
  if Opcodes.is_secondary opcode then
    ""
  else
    Printf.sprintf ", %s" (opr_to_str r)

let rec bytes_to_str b =
  if (Bytes.length b) = 0 then
    ""
  else
    Printf.sprintf "%02X%s"
      (int_of_char (Bytes.get b 0))
      (bytes_to_str (Bytes.sub b 1 ((Bytes.length b) - 1)))

let rec disasm_all ic buf len i eof =
  fill_buffer ic buf len eof;
  let opword, opcode, rd, rs1, rs2, rs2n = decode buf in
  let opl = Bytes.length opword in
  len := !len - opl;
  Bytes.blit buf opl buf 0 !len;
  Bytes.blit eof opl eof 0 !len;
  let s = Printf.sprintf "%06X %s" i (bytes_to_str opword) in
  let l = 30 - String.length s in
  let l = if l < 0 then 2 else l in
  let rs = String.init l (fun i -> ' ') in
  Printf.printf "%s%s%s %s, %s%s\n" s rs
    (Opcodes.get_opcode opcode rs2n) (opr_to_str rd) (opr_to_str rs1) (opr_to_str_opc opcode rs2);
  if (Bytes.get eof 0) = '\x00' then
    disasm_all ic buf len (i + opl) eof

let () =
  let buf_len = 64 in
  let eof = Bytes.create buf_len in
  let buf = Bytes.create buf_len in
  let len = ref 0 in
  Bytes.fill eof 0 buf_len '\x00';
  disasm_all stdin buf len 0 eof
