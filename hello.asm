        jrl rRA, rPC, :main-.
        unimp0
main:   function rZ, :main_end-.-8
        # put address of argument type list into the cif
        add rT1, rPC, :cif-.
        add rT2, rPC, :argt-.
        st64 rT1, rT2, :24
        # put address of the string into the pointer
        add rT3, rPC, :args-.
        add rT0, rPC, :str-.
        add rT4, rPC, :strp-.
        st64 rT4, rT0
        # put address of the pointer in the arglist
        st64 rT3, rT4
        # get address of ffi_type_sint32
        add rT0, rPC, :sint32-.
        dlsym rT0, rZ, rT0
        st64 rT1, rT0, rI16
        # get address of ffi_type_pointer
        add rT0, rPC, :ptr-.
        dlsym rT0, rZ, rT0
        st64 rT2, rT0
        # create cif
        ffimkcif rS0, rT1
        # get address of puts
        add rT0, rPC, :puts-.
        dlsym rS1, rZ, rT0
        # call puts
        fficall rT3, rS0, rS1
        jrl rZ, rRA
        add
main_end:
puts:   db "puts", :0
sint32: db "ffi_type_sint32", :0
ptr:    db "ffi_type_pointer", :0
        align :8
cif:    dq :-1
        dq :1
        dq :0
        dq :0
argt:   dq :0
args:   dq :0
strp:   dq :0
str:    db "Hello, World!", :0
