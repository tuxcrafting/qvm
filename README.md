# QVM

QVM is a simple RISC virtual machine I made to experiment with JIT's.

## Build

QVM uses CMake to build, so it's just creating a build directory and using `cmake` in it.

There are a few options when configuring:

- `NO_VM` - Don't build the VM
- `NO_TOOLS` - Don't build the toolchain
- `NO_FLOAT` - Disable float instructions
- `NO_JIT` - Disable the JIT
- `NO_DL` - Disable `dlsym`
- `NO_FFI` - Disable `ffimkcif` and `fficall`
- `NO_GC` - Disable the garbage collector

You may need to run `make` multiple times after changing the source of QASM's parser or lexer, due to weirdness with the OCaml build system.

After changing `src/opcodes.h`, you need to run `tools/regen.sh` to update QASM and QDIS's opcode tables.

## Programs

- `qvm` - Virtual machine
- `qasm` - Assembler
- `qdis` - Disassembler
- `qalw` - ALW compiler

### QASM

Usage: `qasm < source.asm > binary.bin`

The QVM assembler.  
See `hello.asm` for an example.

### QDIS

Usage: `qdis < source.bin > disassembly.dis`

The QVM disassembler.

### QALW

Usage: `qalw < source.alw > assembly.asm`

ALW compiler. The assembly it emits is slow, big and incomplete, currently.  
See `hello.alw` for an example.

## Documentation

Documentation of the VM is available in comments in `src/opcodes.h`.
