#include "compiler.h"
#include "interpreter.h"
#include "opcodes.h"
#include "util.h"

#ifndef NO_FLOAT
#include <math.h>
#endif

#define BOOL(c) ((c) ? -1 : 0)

uint64_t interpreter_fetch_next(vm_t *vm) {
	uint32_t n = *vm->next_byte++;
	n |= *vm->next_byte++ << 8;
	n |= *vm->next_byte++ << 16;
	n |= *vm->next_byte++ << 24;
	return sext(n, 31);
}

uint64_t interpreter_static_reg(vm_t *vm, uint8_t i) {
	if (i < 8) {
		switch (i) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 4;
		case 4: return 8;
		case 5: return 16;
		case 6: return 3;
		case 7: return 5;
		}
	} else if (i < 16) return -(i - 7);
	return vm->imm;
}

uint64_t interpreter_get_reg(vm_t *vm, uint8_t i) {
	if (IS_STATIC_REG(i)) return interpreter_static_reg(vm, i);
	return vm->registers[i - 16];
}

void interpreter_set_reg(vm_t *vm, uint8_t i, uint64_t n) {
	if (i > 16 && i < 255) vm->registers[i - 16] = n;
}

void interpreter_trap(vm_t *vm, uint64_t type) {
	vm->trap_type = type;
	vm->trap_pc = vm->registers[0];
}

void interpreter_step(vm_t *vm) {
	if (!vm->registers[0]) {
		interpreter_trap(vm, TRAP_ILL);
		return;
	}
	vm->next_byte = (uint8_t*)(uintptr_t)vm->registers[0];
	uint8_t opcode = *vm->next_byte++,
		rd = *vm->next_byte++,
		rs1 = *vm->next_byte++,
		rs2 = *vm->next_byte++;
	if (rd == 255 || rs1 == 255 || rs2 == 255)
		vm->imm = interpreter_fetch_next(vm);
	switch (opcode) {
#define OP2(o, b, t) case o: {	  \
	                     union { \
		                     t v; \
		                     uint64_t u64; \
	                     } rs1_v, rs2_v, rd_v; \
	                     rd_v.u64 = 0; \
	                     rs1_v.u64 = interpreter_get_reg(vm, rs1); \
	                     rs2_v.u64 = interpreter_get_reg(vm, rs2); \
	                     b; \
	                     interpreter_set_reg(vm, rd, rd_v.u64); \
	                     break; \
                     }
#define OP1(o, b, t) case o: {	  \
	                     union { \
		                     t v; \
		                     uint64_t u64; \
	                     } rs1_v, rd_v; \
	                     rd_v.u64 = 0; \
	                     rs1_v.u64 = interpreter_get_reg(vm, rs1); \
	                     b; \
	                     interpreter_set_reg(vm, rd, rd_v.u64); \
	                     break; \
                     }
#define OP1_(o, b, t1, t2) case o: {	  \
	                           union { \
		                           t1 v; \
		                           uint64_t u64; \
	                           } rs1_v; \
	                           union { \
		                           t2 v; \
		                           uint64_t u64; \
	                           } rd_v; \
	                           rd_v.u64 = 0; \
	                           rs1_v.u64 = interpreter_get_reg(vm, rs1); \
	                           b; \
	                           interpreter_set_reg(vm, rd, rd_v.u64); \
	                           break; \
                           }
#define SECONDARY(o, b) case o: {	  \
	                        switch (rs2) { \
		                        b; \
	                        default: \
		                        interpreter_trap(vm, TRAP_ILL); \
	                        } \
	                        break; \
                        }

	case OP_jrl: {
		uint64_t off = interpreter_get_reg(vm, rs1) + interpreter_get_reg(vm, rs2);
		interpreter_set_reg(vm, rd, (uint64_t)(uintptr_t)vm->next_byte);
		vm->next_byte = (uint8_t*)(uintptr_t)off;
		break;
	}

		SECONDARY(0x02,
		          case OP_02_brf: {
			          uint64_t off = interpreter_get_reg(vm, rd);
			          if (interpreter_get_reg(vm, rs1) == 0)
				          vm->next_byte = (uint8_t*)(uintptr_t)(vm->registers[0] + off);
			          break;
		          }
		          case OP_02_brt: {
			          uint64_t off = interpreter_get_reg(vm, rd);
			          if (interpreter_get_reg(vm, rs1) != 0)
				          vm->next_byte = (uint8_t*)(uintptr_t)(vm->registers[0] + off);
			          break;
		          }
		          case OP_02_callnat: {
			          uint8_t *tmp = vm->next_byte;
			          ((native_function_t)(uintptr_t)interpreter_get_reg(vm, rs1))(vm);
			          vm->next_byte = tmp;
			          break;
		          }
		          OP1(OP_02_alloc, rd_v.v = vm_malloc((size_t)rs1_v.v), void*)
		          OP1(OP_02_realloc, rd_v.v = vm_realloc((void*)(uintptr_t)interpreter_get_reg(vm, rd), (size_t)rs1_v.v), void*)
		          OP1(OP_02_free, vm_free(rs1_v.v), void*)
		          case OP_02_function: {
#ifndef NO_JIT
			          native_function_t func = compiler_compile(vm,
			                                                    vm->next_byte,
			                                                    interpreter_get_reg(vm, rs1));
			          if (func != NULL) {
				          vm->next_byte = (uint8_t*)(uintptr_t)vm->registers[0];
				          vm->next_byte[1] = 0;
				          vm->next_byte[2] = 0;
				          vm->next_byte[3] = OP_02_fixjit;
				          *(uint64_t*)&vm->next_byte[4] = (uint64_t)(uintptr_t)func;
			          }
#endif
			          break;
		          }
		          case OP_02_fixjit: {
			          native_function_t func = (native_function_t)(uintptr_t)*(uint64_t*)vm->next_byte;
			          func(vm);
			          vm->next_byte = (uint8_t*)(uintptr_t)vm->registers[238];
			          break;
		          });

		SECONDARY(0x10,
		          OP1(OP_10_abs, rd_v.v = labs(rs1_v.v), int64_t)
		          OP1(OP_10_invert, rd_v.v = ~rs1_v.v, uint64_t)
		          case OP_10_swap: {
			          uint64_t v = interpreter_get_reg(vm, rs1);
			          interpreter_set_reg(vm, rs1, interpreter_get_reg(vm, rd));
			          interpreter_set_reg(vm, rd, v);
			          break;
		          }
		          OP1(OP_10_sext8, rd_v.v = sext(rs1_v.v, 7), uint64_t)
		          OP1(OP_10_sext16, rd_v.v = sext(rs1_v.v, 15), uint64_t)
		          OP1(OP_10_sext32, rd_v.v = sext(rs1_v.v, 31), uint64_t));

		OP2(OP_add, rd_v.v = rs1_v.v + rs2_v.v, uint64_t);
		OP2(OP_sub, rd_v.v = rs1_v.v - rs2_v.v, uint64_t);

		OP2(OP_and, rd_v.v = rs1_v.v & rs2_v.v, uint64_t);
		OP2(OP_or, rd_v.v = rs1_v.v | rs2_v.v, uint64_t);
		OP2(OP_xor, rd_v.v = rs1_v.v ^ rs2_v.v, uint64_t);

		OP2(OP_mul, rd_v.v = rs1_v.v * rs2_v.v, int64_t);
		OP2(OP_div, rd_v.v = rs1_v.v / rs2_v.v, int64_t);
		OP2(OP_rem, rd_v.v = rs1_v.v % rs2_v.v, int64_t);

		OP2(OP_lt, rd_v.v = BOOL(rs1_v.v < rs2_v.v), int64_t);
		OP2(OP_ge, rd_v.v = BOOL(rs1_v.v >= rs2_v.v), int64_t);
		OP2(OP_eq, rd_v.v = BOOL(rs1_v.v == rs2_v.v), uint64_t);
		OP2(OP_neq, rd_v.v = BOOL(rs1_v.v != rs2_v.v), uint64_t);

		OP2(OP_lshift, rd_v.v = rs1_v.v << rs2_v.v, uint64_t);
		OP2(OP_rashift, rd_v.v = rs1_v.v >> rs2_v.v, int64_t);
		OP2(OP_setu, rd_v.v = (rs1_v.v & 0x00000000FFFFFFFFUL) | (rs2_v.v << 32), uint64_t);
		OP2(OP_rlshift, rd_v.v = rs1_v.v >> rs2_v.v, uint64_t);

		OP2(OP_umul, rd_v.v = rs1_v.v * rs2_v.v, uint64_t);
		OP2(OP_udiv, rd_v.v = rs1_v.v / rs2_v.v, uint64_t);
		OP2(OP_urem, rd_v.v = rs1_v.v % rs2_v.v, uint64_t);

		OP2(OP_ult, rd_v.v = BOOL(rs1_v.v < rs2_v.v), uint64_t);
		OP2(OP_uge, rd_v.v = BOOL(rs1_v.v >= rs2_v.v), uint64_t);

#ifndef NO_FLOAT
		SECONDARY(0x30,
		          OP1_(OP_30_f2i, rd_v.v = nearbyint(rs1_v.v), double, int64_t)
		          OP1_(OP_30_i2f, rd_v.v = (double)rs1_v.v, int64_t, double)
		          OP1_(OP_30_f32tof64, rd_v.v = (double)rs1_v.v, float, double)
		          OP1_(OP_30_f64tof32, rd_v.v = (float)rs1_v.v, double, float)
		          OP1(OP_30_flog, rd_v.v = log(rs1_v.v), double)
		          OP1(OP_30_flog10, rd_v.v = log10(rs1_v.v), double)
		          OP1(OP_30_flog2, rd_v.v = log2(rs1_v.v), double)
		          OP1(OP_30_fexp, rd_v.v = exp(rs1_v.v), double)
		          OP1(OP_30_fexp10, rd_v.v = exp10(rs1_v.v), double)
		          OP1(OP_30_fexp2, rd_v.v = exp2(rs1_v.v), double)
		          OP1(OP_30_fsin, rd_v.v = sin(rs1_v.v), double)
		          OP1(OP_30_fcos, rd_v.v = cos(rs1_v.v), double)
		          OP1(OP_30_ftan, rd_v.v = tan(rs1_v.v), double)
		          OP1(OP_30_fasin, rd_v.v = asin(rs1_v.v), double)
		          OP1(OP_30_facos, rd_v.v = acos(rs1_v.v), double)
		          OP1(OP_30_fatan, rd_v.v = atan(rs1_v.v), double)
		          OP1(OP_30_fsinh, rd_v.v = sinh(rs1_v.v), double)
		          OP1(OP_30_fcosh, rd_v.v = cosh(rs1_v.v), double)
		          OP1(OP_30_ftanh, rd_v.v = tanh(rs1_v.v), double)
		          OP1(OP_30_fasinh, rd_v.v = asinh(rs1_v.v), double)
		          OP1(OP_30_facosh, rd_v.v = acosh(rs1_v.v), double)
		          OP1(OP_30_fatanh, rd_v.v = atanh(rs1_v.v), double)
		          OP1(OP_30_fabs, rd_v.v = fabs(rs1_v.v), double)
		          OP1(OP_30_fsqrt, rd_v.v = sqrt(rs1_v.v), double)
		          OP1(OP_30_fcbrt, rd_v.v = cbrt(rs1_v.v), double)
		          OP1_(OP_30_fclassify, rd_v.v = fclassify(rs1_v.v), double, uint64_t));
	case OP_ffma: {
		union {
			double v;
			uint64_t u64;
		} rs1_v, rs2_v, rd_v;
		rd_v.u64 = interpreter_get_reg(vm, rd);
		rs1_v.u64 = interpreter_get_reg(vm, rs1);
		rs2_v.u64 = interpreter_get_reg(vm, rs2);
		rd_v.v = fma(rs1_v.v, rs2_v.v, rd_v.v);
		interpreter_set_reg(vm, rd, rd_v.u64);
		break;
	}
		OP2(OP_fadd, rd_v.v = rs1_v.v + rs2_v.v, double);
		OP2(OP_fsub, rd_v.v = rs1_v.v - rs2_v.v, double);
		OP2(OP_fmul, rd_v.v = rs1_v.v * rs2_v.v, double);
		OP2(OP_fdiv, rd_v.v = rs1_v.v / rs2_v.v, double);
		OP2(OP_frem, rd_v.v = fmod(rs1_v.v, rs2_v.v), double);
		OP2(OP_fpow, rd_v.v = pow(rs1_v.v, rs2_v.v), double);
		OP2(OP_fatan2, rd_v.v = atan2(rs1_v.v, rs2_v.v), double);
		OP2(OP_fpyth, rd_v.v = pyth(rs1_v.v, rs2_v.v), double);
		OP2(OP_flt, rd_v.v = BOOL(rs1_v.v < rs2_v.v), double);
		OP2(OP_fge, rd_v.v = BOOL(rs1_v.v >= rs2_v.v), double);
		OP2(OP_fcopysign, rd_v.v = copysign(rs1_v.v, rs2_v.v), double);
#endif

#define STORE(o, b, t) case o: {	  \
	                       union { \
		                       t v; \
		                       uint64_t u64; \
	                       } rs1_v, rs2_v, rd_v; \
	                       rs1_v.u64 = interpreter_get_reg(vm, rs1); \
	                       rs2_v.u64 = interpreter_get_reg(vm, rs2); \
	                       rd_v.u64 = interpreter_get_reg(vm, rd); \
	                       b; \
	                       break; \
                       }

		STORE(OP_st8,
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v) = (uint8_t)(rs1_v.v & 0xFF), uint64_t);
		STORE(OP_st16,
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v) = (uint8_t)(rs1_v.v & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 1) = (uint8_t)((rs1_v.v >> 8) & 0xFF), uint64_t);
		STORE(OP_st32,
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v) = (uint8_t)(rs1_v.v & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 1) = (uint8_t)((rs1_v.v >> 8) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 2) = (uint8_t)((rs1_v.v >> 16) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 3) = (uint8_t)((rs1_v.v >> 24) & 0xFF), uint64_t);
		STORE(OP_st64,
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v) = (uint8_t)(rs1_v.v & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 1) = (uint8_t)((rs1_v.v >> 8) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 2) = (uint8_t)((rs1_v.v >> 16) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 3) = (uint8_t)((rs1_v.v >> 24) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 4) = (uint8_t)((rs1_v.v >> 32) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 5) = (uint8_t)((rs1_v.v >> 40) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 6) = (uint8_t)((rs1_v.v >> 48) & 0xFF);
		      *(uint8_t*)(uintptr_t)(rd_v.v + rs2_v.v + 7) = (uint8_t)((rs1_v.v >> 56) & 0xFF), uint64_t);

		OP2(OP_ld8,
		    rd_v.v = *(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v), uint64_t);
		OP2(OP_ld16,
		    rd_v.v = (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v);
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 1) << 8, uint64_t);
		OP2(OP_ld32,
		    rd_v.v = (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v);
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 1) << 8;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 2) << 16;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 3) << 24, uint64_t);
		OP2(OP_ld64,
		    rd_v.v = (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v);
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 1) << 8;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 2) << 16;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 3) << 24;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 4) << 32;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 5) << 40;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 6) << 48;
		    rd_v.v |= (uint64_t)*(uint8_t*)(uintptr_t)(rs1_v.v + rs2_v.v + 7) << 56, uint64_t);

#ifndef NO_DL
		OP2(OP_dlsym, rd_v.v = instr_dlsym(rs1_v.v, rs2_v.v), uint64_t);
#endif
#ifndef NO_FFI
		OP1(OP_ffimkcif, rd_v.v = instr_ffimkcif(rs1_v.v), uint64_t);
	case OP_fficall: {
		interpreter_set_reg(vm, rd,
		                    instr_fficall(interpreter_get_reg(vm, rd),
		                                  interpreter_get_reg(vm, rs1),
		                                  interpreter_get_reg(vm, rs2)));
		break;
	}
#endif

	default:
		interpreter_trap(vm, TRAP_ILL);
	}
	vm->registers[0] = (uint64_t)(uintptr_t)vm->next_byte;
}
