#pragma once

#include "vm.h"

uint64_t fclassify(double n);
double exp10(double n);
double pyth(double x, double y);
uint64_t sext(uint64_t n, uint64_t i);

#define vm_new(t) ((t*)vm_malloc(sizeof(t)))
void *vm_malloc(size_t n);
void *vm_realloc(void *p, size_t n);
void vm_free(void *p);

uint64_t instr_dlsym(uint64_t a, uint64_t b);
uint64_t instr_ffimkcif(uint64_t a);
uint64_t instr_fficall(uint64_t r, uint64_t a, uint64_t b);
