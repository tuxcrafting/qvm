#pragma once

#include "vm.h"

uint64_t interpreter_fetch_next(vm_t *vm);
uint64_t interpreter_static_reg(vm_t *vm, uint8_t i);
uint64_t interpreter_get_reg(vm_t *vm, uint8_t i);
void interpreter_set_reg(vm_t *vm, uint8_t i, uint64_t n);
void interpreter_trap(vm_t *vm, uint64_t type);
void interpreter_step(vm_t *vm);
