#pragma once

#include <stdlib.h>

#include "vm.h"

void *compiler_compile(vm_t *vm, void *code, size_t length);
