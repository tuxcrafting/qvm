#include <stdlib.h>

#include "compiler.h"
#include "interpreter.h"
#include "opcodes.h"
#include "util.h"

#ifndef NO_FLOAT
#include <math.h>
#endif

#ifndef NO_JIT
#include <lightning.h>
static jit_state_t *_jit;

void compiler_get_reg(vm_t *vm, int jr, uint8_t i) {
	if (IS_STATIC_REG(i))
		jit_movi(jr, interpreter_static_reg(vm, i));
	else
		jit_ldxi_l(jr, JIT_V0, (i - 16) * sizeof(uint64_t));
}

void compiler_get_reg_f(vm_t *vm, int jr, uint8_t i) {
	if (IS_STATIC_REG(i)) {
		union {
			int64_t u64;
			double f64;
		} v;
		v.u64 = interpreter_static_reg(vm, i);
		jit_movi_d(jr, v.f64);
	} else
		jit_ldxi_d(jr, JIT_V0, (i - 16) * sizeof(uint64_t));
}

void compiler_get_reg_f32(vm_t *vm, int jr, uint8_t i) {
	if (IS_STATIC_REG(i)) {
		union {
			int64_t u64;
			float f32;
		} v;
		v.u64 = interpreter_static_reg(vm, i);
		jit_movi_f(jr, v.f32);
	} else
		jit_ldxi_f(jr, JIT_V0, (i - 16) * sizeof(uint64_t));
}

void compiler_set_reg(int jr, uint8_t i) {
	if (i > 16 && i < 255) {
		jit_movi(JIT_R2, (i - 16) * sizeof(uint64_t));
		jit_stxr_l(JIT_V0, JIT_R2, jr);
	}
}

void compiler_set_reg_f(int jr, uint8_t i) {
	if (i > 16 && i < 255) {
		jit_movi(JIT_R2, (i - 16) * sizeof(uint64_t));
		jit_stxr_d(JIT_V0, JIT_R2, jr);
	}
}

void compiler_set_reg_f32(int jr, uint8_t i) {
	if (i > 16 && i < 255) {
		jit_movi(JIT_R2, (i - 16) * sizeof(uint64_t));
		jit_stxr_f(JIT_V0, JIT_R2, jr);
	}
}

void compiler_fallback(vm_t *vm, uint64_t address) {
	vm->registers[0] = address;
	vm->registers[238] = 0;
	vm->next_byte = (uint8_t*)(uintptr_t)address;
	if (*vm->next_byte == 0x02 && vm->next_byte[3] == OP_02_fixjit) {
		native_function_t f = (native_function_t)(uintptr_t)*(uint64_t*)&vm->next_byte[4];
		f(vm);
		return;
	}
	while (vm->registers[0]) interpreter_step(vm);
}

void compiler_trap(vm_t *vm, int type) {
	interpreter_trap(vm, type);
	compiler_fallback(vm, vm->trap_vectors[type]);
}

int compiler_compile_one(vm_t *vm, size_t base, size_t limit, jit_node_t **labels, ssize_t *instrs) {
	size_t nb = (size_t)vm->next_byte;
	size_t current = instrs[(nb - base) >> 2];
	if (current == (size_t)-1)
		return 0;
	jit_link(labels[current]);
	jit_movi(JIT_R0, 0);
	jit_movi(JIT_R1, nb);
	jit_stxr_l(JIT_V0, JIT_R0, JIT_R1);
	uint8_t opcode = *vm->next_byte++,
		rd = *vm->next_byte++,
		rs1 = *vm->next_byte++,
		rs2 = *vm->next_byte++;
	uint8_t *save = vm->next_byte;
	if (rd == 255 || rs1 == 255 || rs2 == 255)
		vm->imm = interpreter_fetch_next(vm);
	switch (opcode) {
#define TRAP(t) jit_prepare();	  \
		jit_pushargr(JIT_V0); \
		jit_pushargi(t); \
		jit_finishi(interpreter_trap)
#define SECONDARY(o, b) case o: {	  \
	                        switch (rs2) { \
		                        b; \
	                        default: \
		                        TRAP(TRAP_ILL); \
	                        } \
	                        break; \
                        }
#define OP2(o, b) case o: {	  \
	                  compiler_get_reg(vm, JIT_R0, rs1); \
	                  compiler_get_reg(vm, JIT_R1, rs2); \
	                  b; \
	                  compiler_set_reg(JIT_R0, rd); \
	                  break; \
                  }
#define OP1(o, b) case o: {	  \
	                  compiler_get_reg(vm, JIT_R0, rs1); \
	                  b; \
	                  compiler_set_reg(JIT_R0, rd); \
	                  break; \
                  }
#define CALL1(f) jit_prepare();	  \
		jit_pushargr(JIT_R0); \
		jit_finishi(f); \
		jit_retval(JIT_R0)
#define CALL2(f) jit_prepare();	  \
		jit_pushargr(JIT_R0); \
		jit_pushargr(JIT_R1); \
		jit_finishi(f); \
		jit_retval(JIT_R0)

	case OP_jrl: {
		if (rd == 0 && ((rs1 == 0 && rs2 == 254) || (rs1 == 254 && rs2 == 0))) {
			jit_ret();
		} else if ((rs1 == 16 && IS_STATIC_REG(rs2)) || (rs2 == 16 && IS_STATIC_REG(rs1))) {
			size_t cmp = nb + interpreter_static_reg(vm, rs1 == 16 ? rs2 : rs1);
			if (cmp < base || cmp >= limit)
				goto jump_fallback;
			if (rd != 0) return 1;
			jit_node_t *m = jit_jmpi();
			ssize_t ins = instrs[(cmp - base) >> 2];
			if (ins == -1) return 1;
			jit_patch_at(m, labels[ins]);
		} else {
		jump_fallback:
			vm->next_byte = save;
			compiler_get_reg(vm, JIT_R0, rs1);
			compiler_get_reg(vm, JIT_R1, rs2);
			jit_addr(JIT_R0, JIT_R0, JIT_R1);
			jit_prepare();
			jit_pushargr(JIT_V0);
			jit_pushargr(JIT_R0);
			jit_finishi(compiler_fallback);
		}
		break;
	}
#define BRANCH(o, c, ins)	  \
		case o: { \
			jit_node_t *m; \
			if (IS_STATIC_REG(rs1)) { \
				if (interpreter_static_reg(vm, rs1) c 0) \
					break; \
			} else { \
				compiler_get_reg(vm, JIT_R0, rs1); \
				m = jit_b##ins##i(JIT_R0, 0); \
				jit_patch_at(m, labels[current + 1]); \
			} \
			if (!IS_STATIC_REG(rd)) goto branch_fallback; \
			size_t cmp = nb + interpreter_static_reg(vm, rd); \
			if (cmp < base || cmp >= limit) goto branch_fallback; \
			m = jit_jmpi(); \
			ssize_t ins = instrs[(cmp - base) >> 2]; \
			if (ins == -1) return 1; \
			jit_patch_at(m, labels[ins]); \
			break; \
		}
		SECONDARY(0x02,
		          BRANCH(OP_02_brf, !=, ne)
		          BRANCH(OP_02_brt, ==, eq)
		          case OP_02_callnat: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          jit_prepare();
			          jit_pushargr(JIT_V0);
			          jit_finishr(JIT_R0);
			          break;
		          }
		          case OP_02_alloc: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          jit_prepare();
			          jit_pushargr(JIT_R0);
			          jit_finishi(vm_malloc);
			          jit_retval(JIT_R0);
			          compiler_set_reg(JIT_R0, rd);
			          break;
		          }
		          case OP_02_realloc: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          compiler_get_reg(vm, JIT_R1, rd);
			          jit_prepare();
			          jit_pushargr(JIT_R1);
			          jit_pushargr(JIT_R0);
			          jit_finishi(vm_realloc);
			          jit_retval(JIT_R0);
			          compiler_set_reg(JIT_R0, rd);
			          break;
		          }
		          case OP_02_free: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          jit_prepare();
			          jit_pushargr(JIT_R0);
			          jit_finishi(vm_free);
			          break;
		          }
		          case OP_02_function: {
			          if (rd == 255) vm->next_byte += 4;
			          if (rs1 == 255) vm->next_byte += 4;
			          break;
		          }
		          case OP_02_fixjit: {
			          vm->next_byte += 8;
			          break;
		          });

#define SEXT(o, i) OP1(o,	\
                       compiler_get_reg(vm, JIT_R0, rs1);	\
                       jit_prepare();	\
                       jit_pushargr(JIT_R0);	\
                       jit_pushargi(i);	\
                       jit_finishi(sext);	\
                       jit_retval(JIT_R0);	\
                       compiler_set_reg(JIT_R0, rd))

		SECONDARY(0x10,
		          OP1(OP_10_abs, CALL1(labs))
		          OP1(OP_10_invert, jit_comr(JIT_R0, JIT_R0))
		          case OP_10_swap: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          compiler_get_reg(vm, JIT_R1, rd);
			          compiler_set_reg(JIT_R1, rs1);
			          compiler_set_reg(JIT_R0, rd);
			          break;
		          }
		          SEXT(OP_10_sext8, 7)
		          SEXT(OP_10_sext16, 15)
		          SEXT(OP_10_sext32, 31));
		OP2(OP_add, jit_addr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_sub, jit_subr(JIT_R0, JIT_R0, JIT_R1));

		OP2(OP_and, jit_andr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_or, jit_orr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_xor, jit_xorr(JIT_R0, JIT_R0, JIT_R1));

		OP2(OP_mul, jit_mulr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_div, jit_divr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_rem, jit_remr(JIT_R0, JIT_R0, JIT_R1));

#define BOOL(r) jit_lshi(r, r, 63);	  \
		jit_rshi(r, r, 63)

		OP2(OP_lt, jit_ltr(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));
		OP2(OP_ge, jit_ger(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));
		OP2(OP_eq, jit_eqr(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));
		OP2(OP_neq, jit_ner(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));

		OP2(OP_lshift, jit_lshr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_rashift, jit_rshr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_setu,
		    jit_andi(JIT_R0, JIT_R0, 0x00000000FFFFFFFFUL);
		    jit_lshi(JIT_R1, JIT_R1, 32);
		    jit_orr(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_rlshift, jit_rshr_u(JIT_R0, JIT_R0, JIT_R1));

		OP2(OP_umul, jit_qmulr_u(JIT_R0, JIT_R2, JIT_R0, JIT_R1));
		OP2(OP_udiv, jit_divr_u(JIT_R0, JIT_R0, JIT_R1));
		OP2(OP_urem, jit_remr_u(JIT_R0, JIT_R0, JIT_R1));

		OP2(OP_ult, jit_ltr_u(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));
		OP2(OP_uge, jit_ger_u(JIT_R0, JIT_R0, JIT_R1); BOOL(JIT_R0));

#ifndef NO_FLOAT
#define OP2_F(o, b) case o: {	  \
	                    compiler_get_reg_f(vm, JIT_F0, rs1); \
	                    compiler_get_reg_f(vm, JIT_F1, rs2); \
	                    b; \
	                    compiler_set_reg_f(JIT_F0, rd); \
	                    break; \
                    }
#define OP2_FR(o, b) case o: {	  \
	                     compiler_get_reg_f(vm, JIT_F0, rs1); \
	                     compiler_get_reg_f(vm, JIT_F1, rs2); \
	                     b; \
	                     compiler_set_reg(JIT_R0, rd); \
	                     break; \
                     }
#define OP1_F(o, b) case o: {	  \
	                    compiler_get_reg_f(vm, JIT_F0, rs1); \
	                    b; \
	                    compiler_set_reg_f(JIT_F0, rd); \
	                    break; \
                    }

#define FCALL2(f) jit_prepare();	  \
		jit_pushargr_d(JIT_F0); \
		jit_pushargr_d(JIT_F1); \
		jit_finishi(f); \
		jit_retval_d(JIT_F0)
#define FCALL1(f) jit_prepare();	  \
		jit_pushargr_d(JIT_F0); \
		jit_finishi(f); \
		jit_retval_d(JIT_F0)

		SECONDARY(0x30,
		          case OP_30_f2i: {
			          compiler_get_reg_f(vm, JIT_F0, rs1);
			          jit_truncr_d_l(JIT_R0, JIT_F0);
			          compiler_set_reg(JIT_R0, rd);
			          break;
		          }
		          case OP_30_i2f: {
			          compiler_get_reg(vm, JIT_R0, rs1);
			          jit_extr_d(JIT_F0, JIT_R0);
			          compiler_set_reg_f(JIT_F0, rd);
			          break;
		          }
		          case OP_30_f32tof64: {
			          compiler_get_reg_f32(vm, JIT_F0, rs1);
			          jit_extr_f_d(JIT_F0, JIT_F0);
			          compiler_set_reg_f(JIT_F0, rd);
			          break;
		          }
		          case OP_30_f64tof32: {
			          compiler_get_reg_f(vm, JIT_F0, rs1);
			          jit_extr_d_f(JIT_F0, JIT_F0);
			          compiler_set_reg_f32(JIT_F0, rd);
			          break;
		          }

		          OP1_F(OP_30_flog, FCALL1(log))
		          OP1_F(OP_30_flog10, FCALL1(log10))
		          OP1_F(OP_30_flog2, FCALL1(log2))

		          OP1_F(OP_30_fexp, FCALL1(exp))
		          OP1_F(OP_30_fexp10, FCALL1(exp10))
		          OP1_F(OP_30_fexp2, FCALL1(exp2))

		          OP1_F(OP_30_fsin, FCALL1(sin))
		          OP1_F(OP_30_fcos, FCALL1(cos))
		          OP1_F(OP_30_ftan, FCALL1(tan))

		          OP1_F(OP_30_fasin, FCALL1(asin))
		          OP1_F(OP_30_facos, FCALL1(acos))
		          OP1_F(OP_30_fatan, FCALL1(atan))

		          OP1_F(OP_30_fsinh, FCALL1(sinh))
		          OP1_F(OP_30_fcosh, FCALL1(cosh))
		          OP1_F(OP_30_ftanh, FCALL1(tanh))

		          OP1_F(OP_30_fasinh, FCALL1(asinh))
		          OP1_F(OP_30_facosh, FCALL1(acosh))
		          OP1_F(OP_30_fatanh, FCALL1(atanh))

		          OP1_F(OP_30_fabs, jit_absr_d(JIT_F0, JIT_F0))
		          OP1_F(OP_30_fsqrt, jit_sqrtr_d(JIT_F0, JIT_F0))
		          OP1_F(OP_30_fcbrt, FCALL1(cbrt))
		          OP1_F(OP_30_fround, FCALL1(round))
		          OP1_F(OP_30_fclassify, FCALL1(fclassify)));

		OP2_F(OP_fadd, jit_addr_d(JIT_F0, JIT_F0, JIT_F1));
		OP2_F(OP_fsub, jit_subr_d(JIT_F0, JIT_F0, JIT_F1));
		OP2_F(OP_fmul, jit_mulr_d(JIT_F0, JIT_F0, JIT_F1));
		OP2_F(OP_fdiv, jit_divr_d(JIT_F0, JIT_F0, JIT_F1));
		OP2_F(OP_frem, FCALL2(fmod));
	case OP_ffma: {
		compiler_get_reg_f(vm, JIT_F0, rs1);
		compiler_get_reg_f(vm, JIT_F1, rs2);
		compiler_get_reg_f(vm, JIT_F2, rd);
		jit_prepare();
		jit_pushargr_d(JIT_F0);
		jit_pushargr_d(JIT_F1);
		jit_pushargr_d(JIT_F2);
		jit_finishi(fma);
		jit_retval_d(JIT_F0);
		compiler_set_reg_f(JIT_F0, rd);
		break;
	}
		OP2_F(OP_fpow, FCALL2(pow));
		OP2_F(OP_fatan2, FCALL2(atan2));
		OP2_F(OP_fpyth, FCALL2(pyth));
		OP2_FR(OP_flt, jit_ltr_d(JIT_R0, JIT_F0, JIT_F1); BOOL(JIT_R0));
		OP2_FR(OP_fge, jit_ger_d(JIT_R0, JIT_F0, JIT_F1); BOOL(JIT_R0));
		OP2_F(OP_fcopysign, FCALL2(copysign));
#endif

		// TODO explicitely write/read little endian instead of relying on little endian
#define LOAD(o, t)	  \
		case o: { \
			compiler_get_reg(vm, JIT_R0, rs1); \
			compiler_get_reg(vm, JIT_R1, rs2); \
			jit_ldxr_##t(JIT_R0, JIT_R0, JIT_R1); \
			compiler_set_reg(JIT_R0, rd); \
			break; \
		}

		LOAD(OP_ld8, uc);
		LOAD(OP_ld16, us);
		LOAD(OP_ld32, ui);
		LOAD(OP_ld64, l);

#define STORE(o, t)	  \
		case o: { \
			compiler_get_reg(vm, JIT_R0, rd); \
			compiler_get_reg(vm, JIT_R1, rs2); \
			compiler_get_reg(vm, JIT_R2, rs1); \
			jit_stxr_##t(JIT_R0, JIT_R1, JIT_R2); \
			break; \
		}

		STORE(OP_st8, c);
		STORE(OP_st16, s);
		STORE(OP_st32, i);
		STORE(OP_st64, l);

#ifndef NO_DL
		OP2(OP_dlsym, CALL2(instr_dlsym));
#endif
#ifndef NO_FFI
		OP2(OP_ffimkcif, CALL1(instr_ffimkcif));
	case OP_fficall: {
		compiler_get_reg(vm, JIT_R0, rd);
		compiler_get_reg(vm, JIT_R1, rs1);
		compiler_get_reg(vm, JIT_R2, rs2);
		jit_prepare();
		jit_pushargr(JIT_R0);
		jit_pushargr(JIT_R1);
		jit_pushargr(JIT_R2);
		jit_finishi(instr_fficall);
		jit_retval(JIT_R0);
		compiler_set_reg(JIT_R0, rd);
		break;
	}
#endif

	default:
		TRAP(TRAP_ILL);
	}
	return 0;
branch_fallback:
	vm->next_byte = save;
	compiler_get_reg(vm, JIT_R0, rd);
	jit_prepare();
	jit_pushargr(JIT_V0);
	jit_pushargr(JIT_R0);
	jit_finishi(compiler_fallback);
	return 0;
}

void *compiler_compile(vm_t *vm, void *code, size_t length) {
	_jit = jit_new_state();
	jit_prolog();

	jit_node_t *in = jit_arg();
	jit_getarg(JIT_V0, in);

	size_t instrnum = 0;

	uint8_t *tmp = vm->next_byte;
	vm->next_byte = code;
	size_t b = 0, i;
	while (b < length) {
		instrnum++;
		b += 4;
		uint8_t *atmp = vm->next_byte + 1;
		vm->next_byte += 4;
		if (*atmp++ == 255) {
			vm->next_byte += 4;
			b += 4;
		}
		if (*atmp++ == 255) {
			vm->next_byte += 4;
			b += 4;
		}
		if (*atmp == 255) {
			vm->next_byte += 4;
			b += 4;
		}
	}

	jit_node_t **labels = vm_malloc(sizeof(jit_node_t*) * (instrnum + 1));
	for (i = 0; i < instrnum + 1; i++)
		labels[i] = jit_forward();

	ssize_t *instrs = vm_malloc(sizeof(size_t*) * (b + 1));
	for (i = 0; i < b + 1; i++)
		instrs[i] = -1;
	b = 0;

	vm->next_byte = code;
	for (i = 0; i < instrnum; i++) {
		instrs[b] = i;
		b++;
		uint8_t *atmp = vm->next_byte + 1;
		vm->next_byte += 4;
		if (*atmp++ == 255) {
			vm->next_byte += 4;
			b++;
		}
		if (*atmp++ == 255) {
			vm->next_byte += 4;
			b++;
		}
		if (*atmp == 255) {
			vm->next_byte += 4;
			b++;
		}
	}
	instrs[b] = i;
	b *= 4;
	vm->next_byte = code;
	for (i = 0; i < instrnum; i++) {
		if (compiler_compile_one(vm, (size_t)code, (size_t)code + b, labels, instrs)) {
			jit_clear_state();
			jit_destroy_state();
			vm->next_byte = tmp;
			vm_free(labels);
			vm_free(instrs);
			return NULL;
		}
	}
	vm->next_byte = tmp;

	jit_link(labels[instrnum]);
	jit_epilog();

	void *comp = jit_emit();
	jit_clear_state();
//	jit_disassemble();
	vm_free(labels);
	vm_free(instrs);
	return comp;
}
#endif
