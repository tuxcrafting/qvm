#pragma once

#include <stdint.h>

#include "opcodes.h"

typedef struct vm {
	uint64_t registers[239]; // r16-r254
	uint64_t imm;
	uint8_t *next_byte;
	int trap_type;
	uint64_t trap_pc;
	uint64_t trap_vectors[TRAP_MAX];
} vm_t;

typedef void (*native_function_t)(vm_t *vm);

#define IS_STATIC_REG(r) ((r) < 16 || (r) == 255)
