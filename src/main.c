#include <stdio.h>
#include <stdlib.h>

#include "interpreter.h"
#include "util.h"

#ifdef NO_JIT
#define init_jit(_)
#define finish_jit()
#else
#include <lightning.h>
#endif

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "usage: %s bytecode\n", argv[0]);
		return 1;
	}
	FILE *f = fopen(argv[1], "r");
	if (f == NULL) {
		fprintf(stderr, "couldn't open %s for reading\n", argv[1]);
		return 1;
	}
	if (fseek(f, 0, SEEK_END) == -1) {
		fprintf(stderr, "couldn't determine file size\n");
		return 1;
	}
	long size = ftell(f);
	fseek(f, 0, SEEK_SET);
	vm_t *vm = vm_new(vm_t);
	void *code = vm_malloc((size_t)size);
	fread(code, (size_t)size, 1, f);
	fclose(f);
	vm->registers[0] = (uint64_t)(uintptr_t)code;
	init_jit(argv[0]);
	while (!vm->trap_type) {
		interpreter_step(vm);
	}
	finish_jit();
	return 0;
}
