#include <stdlib.h>

#include "opcodes.h"
#include "util.h"

#ifndef NO_FLOAT
#include <math.h>
uint64_t fclassify(double n) {
	if (isnan(n)) return fclassify_NAN;
	else if (isnormal(n)) return fclassify_FINITE;
	else if (!isfinite(n)) return fclassify_INF;
	else if (n == 0.d) return fclassify_ZERO;
	return fclassify_SUBNORMAL;
}

double exp10(double n) {
	return pow(10, n);
}

double pyth(double x, double y) {
	return sqrt(x * x + y * y);
}
#endif

uint64_t sext(uint64_t n, uint64_t i) {
	return n | ((n & (1 << i)) ? ((uint64_t)-1 << i) : 0);
}

#ifdef NO_GC
#include <string.h>
void *vm_malloc(size_t n) {
	void *p = calloc(n + sizeof(size_t), 1);
	*(size_t*)p = n;
	return p + sizeof(size_t);
}
void *vm_realloc(void *p, size_t n) {
	size_t o = *(size_t*)(p - sizeof(size_t));
	p = realloc(p - sizeof(size_t), n + sizeof(size_t));
	*(size_t*)p = n;
	if (n > o)
		memset(p + o + sizeof(size_t), 0, n - o);
	return p;
}
void vm_free(void *p) {
	free(p - sizeof(size_t));
}
#else
#include <gc.h>
void *vm_malloc(size_t n) {
	return GC_MALLOC(n);
}
void *vm_realloc(void *p, size_t n) {
	return GC_REALLOC(p, n);
}
void vm_free(void *p) {
	GC_FREE(p);
}
#endif

struct vm_cif {
	int64_t abi;
	uint64_t nargs;
	uint64_t rtype;
	uint64_t atypes;
};

#ifndef NO_DL
#include <dlfcn.h>
uint64_t instr_dlsym(uint64_t a, uint64_t b) {
	return (uint64_t)(uintptr_t)dlsym(dlopen((const char*)(uintptr_t)a,
	                                         RTLD_LAZY | RTLD_LOCAL),
	                                  (const char*)(uintptr_t)b);
}
#endif

#ifndef NO_FFI
#include <ffi.h>
uint64_t instr_ffimkcif(uint64_t a) {
	struct vm_cif *s = (struct vm_cif*)(uintptr_t)a;
	ffi_abi abi = s->abi == -1 ? FFI_DEFAULT_ABI : (ffi_abi)s->abi;
	unsigned int nargs = (unsigned int)s->nargs;
	ffi_type *rtype = (ffi_type*)(uintptr_t)s->rtype;
	ffi_type **atypes = (ffi_type**)(uintptr_t)s->atypes;
	ffi_cif *cif = vm_new(ffi_cif);
	if (ffi_prep_cif(cif, abi, nargs, rtype, atypes) != FFI_OK)
		return 0;
	return (uint64_t)(uintptr_t)cif;
}
uint64_t instr_fficall(uint64_t r, uint64_t a, uint64_t b) {
	ffi_cif *cif = (ffi_cif*)(uintptr_t)a;
	void *rv = vm_malloc(cif->rtype->size);
	ffi_call(cif, (void*)(uintptr_t)b, rv, (void**)(uintptr_t)r);
	return (uint64_t)(uintptr_t)rv;
}
#endif
