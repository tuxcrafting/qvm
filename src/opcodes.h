#pragma once

/* Instruction encoding:
   oooooooo rrrrrrrr aaaaaaaa bbbbbbbb [iiiiiiii iiiiiiii iiiiiiii iiiiiiii]
   o - opcode
   r - rd
   a - rs1
   b - rs2
   i - immediate (sign-extended little endian 32-bit, optional) */
/* There are 256 64-bit registers, 18 special purpose and 238 general purpose:
   rZ, r0 - Evaluates as 0, discards writes
   rI1, r1 - Evaluates as 1, discards writes
   rI2, r2 - Evaluates as 2, discards writes
   rI4, r3 - Evaluates as 4, discards writes
   rI8, r4 - Evaluates as 8, discards writes
   rI16, r5 - Evaluates as 16, discards writes
   rI3, r6 - Evaluates as 3, discards writes
   rI5, r7 - Evaluates as 5, discards writes
   rN1-rN8, r8-r15 - Evaluates as -1 to -8, discards writes
   rPC, r16 - Address of the current instruction, discards direct writes
   rA0-rA31, r17-r48 - Arguments and return values
   rS0-rS63, r49-r112 - Callee-saved registers
   rT0-rT138, r113-r251 - Caller-saved registers
   rSP, r252 - Stack pointer
   rFP, r253 - Frame pointer
   rRA, r254 - Return address
   rIMM, r255 - Evaluates as the instruction's immediate field, discards writes */
/* ABI:
   Arguments are passed in rA0-rA31, spilling on the stack in reverse order if needed.
   Return values are passed in rA0-rA31. */

/* raise(ILL) */
#define OP_unimp0 0x00
/* rd = rPC
   rPC = rs1 + rs2 */
#define OP_jrl 0x01
/* if (rs1 == 0) { rPC = rPC + rd } */
#define OP_02_brf 0x00
/* if (rs1 != 0) { rPC = rPC + rd } */
#define OP_02_brt 0x01
/* rs1() */
#define OP_02_callnat 0x08
/* rd = malloc(rs1) */
#define OP_02_alloc 0x10
/* rd = realloc(rd, rs1) */
#define OP_02_realloc 0x11
/* free(rs1) */
#define OP_02_free 0x12
/* compile(rPC, rPC + rs1) */
#define OP_02_function 0x18
/* you should never use this instruction */
#define OP_02_fixjit 0x19
/* rd = abs(rs1) */
#define OP_10_abs 0x00
/* rd = ~rs1 */
#define OP_10_invert 0x01
/* rd <=> rs1 */
#define OP_10_swap 0x02
/* rd = sext(rs1, 7) */
#define OP_10_sext8 0x08
/* rd = sext(rs1, 15) */
#define OP_10_sext16 0x09
/* rd = sext(rs1, 31) */
#define OP_10_sext32 0x0A
/* rd = rs1 + rs2 */
#define OP_add 0x11
/* rd = rs1 - rs2 */
#define OP_sub 0x12
/* rd = rs1 & rs2 */
#define OP_and 0x13
/* rd = rs1 | rs2 */
#define OP_or 0x14
/* rd = rs1 ^ rs2 */
#define OP_xor 0x15
/* rd = rs1 * rs2 */
#define OP_mul 0x16
/* rd = rs1 / rs2 */
#define OP_div 0x17
/* rd = rs1 % rs2 */
#define OP_rem 0x18
/* rd = rs1 < rs2 */
#define OP_lt 0x19
/* rd = rs1 >= rs2 */
#define OP_ge 0x1A
/* rd = rs1 == rs2 */
#define OP_eq 0x1B
/* rd = rs1 != rs2 */
#define OP_neq 0x1C
/* rd = rs1 lsl rs2 */
#define OP_lshift 0x1D
/* rd = rs1 asr rs2 */
#define OP_rashift 0x1E
/* rd = (rs1 & 0xFFFFFFFF) | (rs2 lsl 32) */
#define OP_setu 0x1F
/* rd = rs1 lsr rs2 */
#define OP_rlshift 0x20
/* rd = rs1_u * rs2_u */
#define OP_umul 0x21
/* rd = rs1_u / rs2_u */
#define OP_udiv 0x22
/* rd = rs1_u % rs2_u */
#define OP_urem 0x23
/* rd = rs1_u < rs2_u */
#define OP_ult 0x24
/* rd = rs1_u >= rs2_u */
#define OP_uge 0x25
/* rd = to_int(rs1_f) */
#define OP_30_f2i 0x00
/* rd_f = to_float(rs1) */
#define OP_30_i2f 0x01
/* rd_f = to_float(rs1_f32) */
#define OP_30_f32tof64 0x02
/* rd_f32 = to_float32(rs1_f) */
#define OP_30_f64tof32 0x03
/* rd_f = log(e, rs1_f) */
#define OP_30_flog 0x10
/* rd_f = log(10, rs1_f) */
#define OP_30_flog10 0x11
/* rd_f = log(2, rs1_f) */
#define OP_30_flog2 0x12
/* rd_f = pow(e, rs1_f) */
#define OP_30_fexp 0x14
/* rd_f = pow(10, rs1_f) */
#define OP_30_fexp10 0x15
/* rd_f = pow(2, rs1_f) */
#define OP_30_fexp2 0x16
/* rd_f = sin(rs1_f) */
#define OP_30_fsin 0x18
/* rd_f = cos(rs1_f) */
#define OP_30_fcos 0x19
/* rd_f = tan(rs1_f) */
#define OP_30_ftan 0x1A
/* rd_f = asin(rs1_f) */
#define OP_30_fasin 0x1C
/* rd_f = acos(rs1_f) */
#define OP_30_facos 0x1D
/* rd_f = atan(rs1_f) */
#define OP_30_fatan 0x1E
/* rd_f = sinh(rs1_f) */
#define OP_30_fsinh 0x20
/* rd_f = cosh(rs1_f) */
#define OP_30_fcosh 0x21
/* rd_f = tanh(rs1_f) */
#define OP_30_ftanh 0x22
/* rd_f = asinh(rs1_f) */
#define OP_30_fasinh 0x24
/* rd_f = acosh(rs1_f) */
#define OP_30_facosh 0x25
/* rd_f = atanh(rs1_f) */
#define OP_30_fatanh 0x26
/* rd_f = abs(rs1_f) */
#define OP_30_fabs 0x28
/* rd_f = sqrt(rs1_f) */
#define OP_30_fsqrt 0x29
/* rd_f = cbrt(rs1_f) */
#define OP_30_fcbrt 0x2A
/* rd_f = round(rs1_f) */
#define OP_30_fround 0x2B
/* rd_f = classify(rs1_f) */
#define OP_30_fclassify 0x30
/* rd_f = rs1_f + rs2_f */
#define OP_fadd 0x31
/* rd_f = rs1_f - rs2_f */
#define OP_fsub 0x32
/* rd_f = rs1_f * rs2_f */
#define OP_fmul 0x33
/* rd_f = rs1_f / rs2_f */
#define OP_fdiv 0x34
/* rd_f = rs1_f % rs2_f */
#define OP_frem 0x35
/* rd_f = rd_f + rs1_f * rs2_f */
#define OP_ffma 0x36
/* rd_f = pow(rs1_f, rs2_f) */
#define OP_fpow 0x37
/* rd_f = atan2(rs1_f, rs2_f) */
#define OP_fatan2 0x38
/* rd_f = sqrt(rs1_f * rs1_f + rs2_f * rs2_f) */
#define OP_fpyth 0x39
/* rd = rs1_f < rs2_f */
#define OP_flt 0x3A
/* rd = rs1_f >= rs2_f */
#define OP_fge 0x3B
/* rd = copysign(rs1_f, rs2_f) */
#define OP_fcopysign 0x3C
/* *(rd + rs2) = rs1_8 */
#define OP_st8 0x40
/* *(rd + rs2) = rs1_16 */
#define OP_st16 0x41
/* *(rd + rs2) = rs1_32 */
#define OP_st32 0x42
/* *(rd + rs2) = rs1 */
#define OP_st64 0x43
/* rd_8 = *(rs1 + rs2) */
#define OP_ld8 0x44
/* rd_16 = *(rs1 + rs2) */
#define OP_ld16 0x45
/* rd_32 = *(rs1 + rs2) */
#define OP_ld32 0x46
/* rd = *(rs1 + rs2) */
#define OP_ld64 0x47
/* rd = dlsym(dlopen(rs1, RTLD_LAZY | RTLD_LOCAL), rs2) */
#define OP_dlsym 0x50
/* rd = malloc(sizeof(ffi_cif))
   abi = if (*rs1 == -1) { FFI_DEFAULT_ABI } else { *rs1 }
   ffi_prep_cif(rd, abi, *(rs1 + 8), *(rs1 + 16), *(rs1 + 24)) */
#define OP_ffimkcif 0x51
/* r = malloc(rs1->rtype->size)
   ffi_call(rs1, rs2, r, rd)
   rd = r */
#define OP_fficall 0x52

#define fclassify_ZERO (1<<0)
#define fclassify_FINITE (1<<1)
#define fclassify_SUBNORMAL (1<<2)
#define fclassify_INF (1<<3)
#define fclassify_NAN (1<<4)

enum {
	TRAP_NONE = 0,
	TRAP_ILL, // illegal instruction executed
	TRAP_MAX,
};
